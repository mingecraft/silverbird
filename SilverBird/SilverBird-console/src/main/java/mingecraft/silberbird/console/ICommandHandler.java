package mingecraft.silberbird.console;

/**
 * Handler interface for console commands
 * 
 * @author maxoufox
 *
 */
public interface ICommandHandler {
	/**
	 * Handle a command
	 * 
	 * @param args The args passed to the command, excluding the command name
	 * @return true if everything went right
	 */
	public boolean handle(String[] args);

	/**
	 * Get the command name
	 * 
	 * @return command name
	 */
	public String getCommand();

	/**
	 * Get the command usage
	 * 
	 * @return command usage
	 */
	public String getUsage();
}
