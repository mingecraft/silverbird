package mingecraft.silberbird.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.net.util.interfaces.ICleanable;

public class Console implements ICleanable {
	private BufferedReader in;
	private Logger l = Logger.getLogger("Console");
	private HashMap<String, ICommandHandler> commands = new HashMap<String, ICommandHandler>();
	private boolean run = true;
	
	public Console(InputStream is) {
		in = new BufferedReader(new InputStreamReader(is));
	}
	
	public void addCommand(ICommandHandler command) {
		commands.put(command.getCommand(), command);
	}
	
	public void run() {
		l.i("Console handler started.");
		String s;
		
		while(run) {
			try {
				s = in.readLine();
				
				String[] words = s.split(" ");
				
				String command = "";
				String[] args = {};
				
				if (words.length >= 1) {
					command = words[0];
				}
				
				if (words.length >= 2) {
					args = new String[words.length - 1];
					for(int i = 1; i < words.length; i++) {
						args[i - 1] = words[i];
					}
				}
				
				if (commands.containsKey(command)) {
					ICommandHandler c = ICommandHandler.class.cast(commands.get(command));
					
					if (!c.handle(args)) {
						l.e("Usage: " + c.getUsage() + ".");
					}
				} else {
					l.e("Unkwnow command " + command + "!");
				}
				
				
			} catch (IOException e) {
				l.e("Error while reading command: ", e);
			}
		}
		l.i("Console handler stoped.");
	}

	@Override
	public void clean() {
		run = false;
		try {
			in.close();
		} catch (IOException e) {
		}
	}
}
