package mingecraft.silverbird.net.util.interfaces;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Interface to handle new connection.
 * 
 * @author maxoufox
 *
 */
public abstract class INetManager implements ICleanable {
	public DataInputStream dis;
	public DataOutputStream dos;
	protected Socket client;

	/**
	 * Init.
	 * 
	 * @param client Client socket
	 * @throws IOException If error
	 */
	public void init(Socket client) throws IOException {
		this.client = client;
		
		this.dis = new DataInputStream(this.client.getInputStream());
		this.dos = new DataOutputStream(this.client.getOutputStream());
	}

	/**
	 * Handle a new socket.
	 * 
	 * @param client Client socket.
	 */
	public abstract void handle();
	
	/**
	 * Close the connection
	 * @throws IOException If error.
	 */
	public void close() throws IOException {
		client.close();
	}

}
