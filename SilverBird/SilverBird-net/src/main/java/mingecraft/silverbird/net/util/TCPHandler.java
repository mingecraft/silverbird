package mingecraft.silverbird.net.util;

import java.io.IOException;
import java.net.Socket;

import mingecraft.silverbird.net.TCPServer;
import mingecraft.silverbird.net.util.interfaces.ICleanable;
import mingecraft.silverbird.net.util.interfaces.INetManager;

public class TCPHandler extends Thread implements ICleanable {
	private INetManager handler;
	private Socket socket;
	private Logger l = Logger.getLogger("TCPHandler");
	
	public TCPHandler(INetManager handler, Socket socket) {
		this.handler = handler;
		this.socket = socket;
	}
	
	public void run() {
		try {
			this.handler.init(this.socket);
		} catch (IOException e) {
			l.e("Can't init handler!", e);
			return;
		}
		this.handler.handle();
	}
	
	public void clean() {
		this.handler.clean();
		TCPServer.getInstance();
	}
}
