package mingecraft.silverbird.net.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;

/**
 * Utility class to log things.
 * 
 * @author maxoufox
 *
 */
public class Logger {
	/**
	 * Stores loggers.
	 */
	private static HashMap<String, Logger> loggers = new HashMap<String, Logger>();

	/**
	 * Logger name.
	 */
	private String name;

	/**
	 * Class used to display stackTraces properly.
	 * 
	 * @author maxoufox
	 *
	 */
	private static class TraceOutputStream extends OutputStream {
		/**
		 * String builder to store stackTraces.
		 */
		private StringBuilder string = new StringBuilder();

		/**
		 * Write a byte to the string builder.
		 */
		@Override
		public void write(int b) throws IOException {
			this.string.append((char) b);
		}

		/**
		 * Get the content of the string builder.
		 */
		public String toString() {
			return this.string.toString();
		}
	}

	/**
	 * Severity of the log.
	 * 
	 * DEBUG : Development things, not useful for the end user. INFO : Basic
	 * logging, tells what's happening. WARN : Something not intended but not
	 * important has append. ERROR : Something not intended and important has
	 * append. FATAL : Something not intended has append, software must stop.
	 * 
	 * @author maxoufox
	 *
	 */
	public static enum Severity {
		DEBUG(0, "DBUG"), INFO(1, "INFO"), WARNING(2, "WARN"), ERROR(3, "ERR!"), FATAL(4, "FTAL");

		private int id = -1;
		private String name = "NULL";

		/**
		 * Constructor
		 * 
		 * @param id   Numerical id
		 * @param name Name disaplyed
		 */
		private Severity(int id, String name) {
			this.id = id;
			this.name = name;
		}

		/**
		 * Get Severity id
		 * 
		 * @return Numerical id of the severity level.
		 */
		public int getId() {
			return this.id;
		}

		/**
		 * Get the name
		 * 
		 * @return The display name of the severity level.
		 */
		public String getName() {
			return this.name;
		}
	}

	/**
	 * Constructor
	 * 
	 * @param name The name of the logger.
	 */
	private Logger(String name) {
		this.name = name;
	}

	/**
	 * Debug text.
	 * 
	 * @param text Text to debug.
	 */
	public void d(String text) {
		log(Severity.DEBUG, text);
	}

	/**
	 * Debug text with exception.
	 * 
	 * @param text Text to debug.
	 * @param t    Exception to debug.
	 */
	public void d(String text, Throwable t) {
		log(Severity.DEBUG, text, t);
	}

	/**
	 * Debugs an object.
	 * 
	 * @param text Text to print
	 * @param o    Object to debug. Just calls o.toString().
	 */
	public void d(String text, Object o) {
		log(Severity.DEBUG, text + o.toString());
	}

	/**
	 * Print as info.
	 * 
	 * @param text Text to print.
	 */
	public void i(String text) {
		log(Severity.INFO, text);
	}

	/**
	 * Print as info with an exception.
	 * 
	 * @param text Text to print.
	 * @param t    Exception to print.
	 */
	public void i(String text, Throwable t) {
		log(Severity.INFO, text, t);
	}

	/**
	 * Print as warning.
	 * 
	 * @param text Text to print.
	 */
	public void w(String text) {
		log(Severity.WARNING, text);
	}

	/**
	 * Print as warning with an exception.
	 * 
	 * @param text Text to print.
	 * @param t    Exception to print.
	 */
	public void w(String text, Throwable t) {
		log(Severity.WARNING, text, t);
	}

	/**
	 * Print as error.
	 * 
	 * @param text Text to print.
	 */
	public void e(String text) {
		log(Severity.ERROR, text);
	}

	/**
	 * Print as error with an exception.
	 * 
	 * @param text Text to print.
	 * @param t    Exception to print.
	 */
	public void e(String text, Throwable t) {
		log(Severity.ERROR, text, t);
	}

	/**
	 * Print as fatal.
	 * 
	 * @param text Text to print.
	 */
	public void f(String text) {
		log(Severity.FATAL, text);
	}

	/**
	 * Print as fatal with an exception.
	 * 
	 * @param text Text to print.
	 * @param t    Exception to print.
	 */
	public void f(String text, Throwable t) {
		log(Severity.FATAL, text, t);
	}

	/**
	 * Log text.
	 * 
	 * @param s    Severity level of the logged text.
	 * @param text Text to log.
	 */
	public void log(Severity s, String text) {
		System.out.println("[" + TimeHelper.formatDateTime() + "][" + name + "][" + s.getName() + "] " + text);
	}

	/**
	 * Log text with an exception.
	 * 
	 * @param s    Severity of the logged text.
	 * @param text Text to log.
	 * @param e    Exception to trace.
	 */
	public void log(Severity s, String text, Throwable e) {
		this.log(s, text);

		TraceOutputStream tos = new TraceOutputStream();

		e.printStackTrace(new PrintStream(tos));

		String[] trace = tos.toString().split(System.lineSeparator());

		for (String line : trace) {
			System.out.println("[" + TimeHelper.formatDateTime() + "][" + name + "][" + s.getName() + "] " + line);
		}
	}

	/**
	 * Get a logger by it's name, if the logger with name {@value name} doesn't
	 * exists, create it.
	 * 
	 * @param name The name of the logger.
	 * @return A logger.
	 */
	public static Logger getLogger(String name) {
		if (!loggers.containsKey(name))
			loggers.put(name, new Logger(name));

		return loggers.get(name);
	}

}
