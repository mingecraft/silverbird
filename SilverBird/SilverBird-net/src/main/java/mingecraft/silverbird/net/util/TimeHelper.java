package mingecraft.silverbird.net.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Provides functions for working with date and time.
 * 
 * @author maxoufox
 */

public class TimeHelper {
	private static SimpleDateFormat m_momentFormat = new SimpleDateFormat("dd/MM/yyyy HH:MM:ss");
	
	/**
	 * @return current time in ms
	 */
	public static long getTime() {
		return System.currentTimeMillis();
	}
	
	/**
	 * @return a Date representing the current time.
	 */
	public static Date getDateTime() {
		return new Date(TimeHelper.getTime());
	}
	
	/**
	 * @return formatted time in a String
	 */
	public static String formatDateTime() {
		return TimeHelper.formatDateTime(TimeHelper.getDateTime());
	}
	
	/**
	 * @param d
	 *            The Date to format
	 * @return The date formatted in a String.
	 */
	public static String formatDateTime(Date d) {
		return m_momentFormat.format(d);
	}
	
	/**
	 * @param d
	 *            A Date
	 * @return Get the time elapsed between {@code d} and now.
	 */
	public static long calculateDaysElapsed(Date d) {
		long diff = TimeHelper.getTime() - d.getTime();
		long diffDays = diff / (24 * 60 * 60 * 1000);
		return diffDays;
	}
	
	/**
	 * @return the current date formatter;
	 */
	public static SimpleDateFormat getMomentFormatter() {
		return m_momentFormat;
	}
}