package mingecraft.silverbird.net.util.interfaces;

/**
 * Interface representing cleneable object.
 * 
 * @author maxoufox
 *
 */
public interface ICleanable {

	/**
	 * Called when no need for object anymore.
	 * Must be blocking.
	 */
	public void clean();

}
