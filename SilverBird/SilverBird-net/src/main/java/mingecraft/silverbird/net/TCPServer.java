package mingecraft.silverbird.net;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.net.util.TCPHandler;
import mingecraft.silverbird.net.util.interfaces.ICleanable;
import mingecraft.silverbird.net.util.interfaces.INetManager;

/**
 * TCP Server handler
 * 
 * @author maxoufox
 *
 */
public class TCPServer extends Thread implements ICleanable {
	private static TCPServer instance = null;
	
	private ServerSocket server;
	private Logger l = Logger.getLogger("Net");

	private ArrayList<TCPHandler> handlers = new ArrayList<TCPHandler>();
	private Class<? extends INetManager> handler;

	private String ip;
	private int port;

	private boolean mustStop = false;
	private boolean running = false;

	/**
	 * Create a TCP Server
	 * 
	 * @param ip   IP to bind to
	 * @param port Port to bind
	 */
	public TCPServer(String ip, int port, Class<? extends INetManager> handler) {
		this.ip = ip;
		this.port = port;
		this.handler = handler;
	}

	/**
	 * Check if the server is running.
	 * 
	 * @return Running or not.
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * Stops the server and wait until it's stopped.
	 */
	public void clean() {
		this.mustStop = true;
		l.i("Stopping TCP Server...");

		try {
			this.join();
		} catch (InterruptedException e) {
			l.e("Error while stopping TCP Server :", e);
		}

		l.i("OK!");
	}
	
	public static TCPServer getInstance() {
		return instance;
	}

	public void run() {
		TCPServer.instance = this;
		
		l.i("Starting TCP server on " + ip + ":" + port + "...");
		running = true;

		try {
			server = new ServerSocket();
			server.setReuseAddress(true);
			server.setSoTimeout(1000);
			server.bind(new InetSocketAddress(ip, port));
			l.i("OK!");
		} catch (IOException e) {
			l.f("Can't start TCP server :", e);
			System.exit(-1);
		}

		while (!mustStop) {
			try {
				Socket socket = server.accept();

				INetManager handler = this.handler.newInstance();
				TCPHandler tcphandler = new TCPHandler(handler, socket);
				
				handlers.add(tcphandler);
				tcphandler.start();
			} catch (SocketTimeoutException e) {
			} catch (InstantiationException | IllegalAccessException | IOException e2) {
				l.e("A net error has happend! ", e2);
			}
		}

		running = false;
	}
	
	public void removeHandler(TCPHandler tcphandler) {
		int id = handlers.indexOf(tcphandler);
		
		if (id != -1) {
			handlers.remove(id);
		} else {
			l.e("Trying to remove non existing handler...?");
		}
	}
}
