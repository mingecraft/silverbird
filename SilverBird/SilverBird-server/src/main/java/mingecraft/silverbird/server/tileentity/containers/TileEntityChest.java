package mingecraft.silverbird.server.tileentity.containers;

import java.util.ArrayList;
import java.util.List;

import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.CompoundTag;

import mingecraft.silverbird.server.container.IInventory;
import mingecraft.silverbird.server.container.ItemStackInv;
import mingecraft.silverbird.server.entity.INBTSavable;
import mingecraft.silverbird.server.tileentity.TileEntity;

public class TileEntityChest extends TileEntity implements INBTSavable, IInventory {

	public TileEntityChest() {
		super();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void load(CompoundMap data) {
		super.load(data);
		
		content = new ItemStackInv[getSize()];
		
		List<CompoundTag> items = (List<CompoundTag>) data.get("Items").getValue();
		
		for(CompoundTag i : items) {
			ItemStackInv item = new ItemStackInv();
			item.load(i.getValue());
			
			content[item.slot] = item;
		}
	}
	
	public CompoundMap save() {
		CompoundMap out = super.save();
		
		List<CompoundTag> items = new ArrayList<CompoundTag>();
		
		for(ItemStackInv i : content) {
			if (i == null)
				continue;
			
			items.add(new CompoundTag("", i.save()));
		}
		
		return out;
	}
	
	@Override
	public int getSize() {
		return 27;
	}

	@Override
	public ItemStackInv getItemInSlot(int slot) {
		return content[slot];
	}

	@Override
	public void setItem(ItemStackInv item) {
		content[item.slot] = item;
	}
	
	private ItemStackInv[] content;

	@Override
	public int getType() {
		return 0;
	}

	@Override
	public void destroyItem(int slot) {
		content[slot] = null;
	}

}
