package mingecraft.silverbird.server.world.chunks;

public final class ChunkCoordinates {
	public ChunkCoordinates(int i, int j) {
		x = i;
		z = j;
	}

	public boolean equals(Object obj) {
		if (obj instanceof ChunkCoordinates) {
			ChunkCoordinates chunkcoordinates = (ChunkCoordinates) obj;
			return x == chunkcoordinates.x && z == chunkcoordinates.z;
		} else {
			return false;
		}
	}

	public int hashCode() {
		return x << 16 ^ z;
	}

	public final int x;
	public final int z;
}
