package mingecraft.silverbird.server.container;

import com.flowpowered.nbt.ByteTag;
import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.ShortTag;

import mingecraft.silverbird.server.entity.INBTSavable;

public class ItemStack implements INBTSavable {

	@Override
	public void load(CompoundMap data) {
		id = (short) data.get("id").getValue();
		damage = (short) data.get("Damage").getValue();
		count = (byte) data.get("Count").getValue();
	}

	@Override
	public CompoundMap save() {
		CompoundMap out = new CompoundMap();

		out.put(new ShortTag("id", id));
		out.put(new ShortTag("Damage", damage));
		out.put(new ByteTag("Count", count));
		
		return out;
	}

	public short id;
	public short damage;
	public byte count;
}
