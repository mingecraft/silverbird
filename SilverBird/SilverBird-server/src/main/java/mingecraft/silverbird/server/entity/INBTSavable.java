package mingecraft.silverbird.server.entity;

import com.flowpowered.nbt.CompoundMap;

/**
 * Describes an element which can be loaded into an NBT tag
 * @author maxoufox
 *
 */
public interface INBTSavable {
	
	/**
	 * Load from the NBT tag.
	 * @param data Data to load from
	 */
	public void load(CompoundMap data);
	/**
	 * Saves into a NBT tag
	 * @return A CompoundMap.
	 */
	public CompoundMap save();
}
