package mingecraft.silverbird.server.entity.mob;

import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.IntTag;

import mingecraft.silverbird.server.entity.Entity;
import mingecraft.silverbird.server.entity.INBTSavable;

public class EntitySlime extends Entity implements INBTSavable {
	public EntitySlime() {
		super();
	}
	
	public void load(CompoundMap nbt) {
		super.load(nbt);

		size = (int) nbt.get("Size").getValue();
	}
	
	public CompoundMap save() {
		CompoundMap out = super.save();
		
		out.put(new IntTag("Size", size));
		
		return out;
	}
	
	public int size;
}
