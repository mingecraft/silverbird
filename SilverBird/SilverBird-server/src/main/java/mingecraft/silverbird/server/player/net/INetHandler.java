package mingecraft.silverbird.server.player.net;

import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.protocol.Packet;

/**
 * Used to handle client packets.
 * 
 * @author maxoufox
 *
 */
public interface INetHandler {
	/**
	 * Handle an incoming packet
	 * 
	 * @param packet The packet.
	 * @param player The player.
	 * @throws IOException 
	 */
	void handle(Packet packet, Player player) throws IOException;
}
