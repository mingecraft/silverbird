package mingecraft.silverbird.server.protocol.packets.entities.movement;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet21EntityLookRelMove extends Packet {

	int eid;
	byte x, y, z;
	byte yaw, pitch;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		eid = dis.readInt();
		x = dis.readByte();
		y = dis.readByte();
		z = dis.readByte();
		yaw = dis.readByte();
		pitch = dis.readByte();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(eid);
		dos.writeByte(x);
		dos.writeByte(y);
		dos.writeByte(z);
		dos.writeByte(yaw);
		dos.writeByte(pitch);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 9;
	}

	@Override
	public int getPacketId() {
		return 0x21;
	}

	@Override
	public String debug() {
		return "EntityLookRelMove [eid: " + eid + "; x: " + x + "; y: " + y + "; z: " + z + "; yaw: " + yaw + "; pitch: " + pitch + "]";
	}

}