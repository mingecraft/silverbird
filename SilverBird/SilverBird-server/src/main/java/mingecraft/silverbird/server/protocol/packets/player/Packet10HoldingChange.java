package mingecraft.silverbird.server.protocol.packets.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet10HoldingChange extends Packet {

	int eid;
	short id;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		eid = dis.readInt();
		id = dis.readShort();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(eid);
		dos.writeShort(id);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 6;
	}

	@Override
	public int getPacketId() {
		return 0x10;
	}

	@Override
	public String debug() {
		return "HoldingChange [eid: " + eid + "; id: " + id + "]";
	}

}
