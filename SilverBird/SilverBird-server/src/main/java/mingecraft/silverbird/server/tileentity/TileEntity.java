package mingecraft.silverbird.server.tileentity;

import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.IntTag;

import mingecraft.silverbird.server.entity.INBTSavable;

public class TileEntity implements INBTSavable {

	@Override
	public void load(CompoundMap data) {
		x = (int) data.get("x").getValue();
		y = (int) data.get("y").getValue();
		z = (int) data.get("z").getValue();
	}

	@Override
	public CompoundMap save() {
		CompoundMap out = new CompoundMap();

		out.put(new IntTag("x", x));
		out.put(new IntTag("y", y));
		out.put(new IntTag("z", z));
		
		return out;
	}
	
	public int x, y, z;

}
