package mingecraft.silverbird.server.entity.items;

import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.CompoundTag;

import mingecraft.silverbird.server.container.ItemStack;
import mingecraft.silverbird.server.entity.Entity;
import mingecraft.silverbird.server.entity.INBTSavable;

public class EntityItem extends Entity implements INBTSavable {
	public EntityItem() {
		
	}
	
	public void load(CompoundMap nbt) {
		super.load(nbt);

		health = (short) nbt.get("Health").getValue();
		age = (short) nbt.get("Age").getValue();
		
		item = new ItemStack();
		item.load(((CompoundTag)nbt.get("Item")).getValue());
	}
	
	public CompoundMap save() {
		CompoundMap out = super.save();
		
		out.put(new CompoundTag("Item", item.save()));
		
		return out;
	}
	
	public short health;
	public short age;
	public ItemStack item;
}
