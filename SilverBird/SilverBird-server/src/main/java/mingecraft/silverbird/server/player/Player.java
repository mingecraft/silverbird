package mingecraft.silverbird.server.player;

import java.io.IOException;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.server.NetManager;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.player.net.LoginNetHandler;
import mingecraft.silverbird.server.protocol.Packet;
import mingecraft.silverbird.server.protocol.packets.player.PacketFFKick;

public class Player {
	public String name;
	public NetManager netManager;
	public INetHandler netHandler;
	
	private Logger pl = Logger.getLogger("Packet");

	/**
	 * Init the player
	 * 
	 * @param netManager Connection's net manager.
	 */
	public Player(NetManager netManager) {
		this.netManager = netManager;
		this.netHandler = new LoginNetHandler();
	}

	/**
	 * Send a packet to the player
	 * 
	 * @param p Packet to send
	 * @throws IOException If error
	 */
	public void sendPacket(Packet p) throws IOException {
		netManager.dos.writeByte(p.getPacketId());
		
		pl.d("[S->C] " + p.debug());
		
		p.writePacketData(netManager.dos);
	}

	/**
	 * Kicks the player.
	 * 
	 * @param reason Message to be displayed to the client.
	 * @throws IOException If error.
	 */
	public void kick(String reason) throws IOException {
		PacketFFKick kick = new PacketFFKick();
		
		kick.reason = reason;
		
		this.sendPacket(kick);
		this.netManager.online = false;
		netManager.close();
	}
}
