package mingecraft.silverbird.server.protocol.packets.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet08UpdateHealth extends Packet {

	byte health;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		health = dis.readByte();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeByte(health);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 1;
	}

	@Override
	public int getPacketId() {
		return 8;
	}

	@Override
	public String debug() {
		return "Health [" + health + "]";
	}

}
