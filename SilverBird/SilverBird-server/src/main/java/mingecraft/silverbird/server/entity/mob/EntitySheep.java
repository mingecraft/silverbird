package mingecraft.silverbird.server.entity.mob;

import com.flowpowered.nbt.ByteTag;
import com.flowpowered.nbt.CompoundMap;

import mingecraft.silverbird.server.entity.Entity;
import mingecraft.silverbird.server.entity.INBTSavable;

public class EntitySheep extends Entity implements INBTSavable {
	public EntitySheep() {
		super();
	}
	
	public void load(CompoundMap nbt) {
		super.load(nbt);

		sheared = (byte) nbt.get("Sheared").getValue() != 0;
	}
	
	public CompoundMap save() {
		CompoundMap out = super.save();
		
		out.put(new ByteTag("Sheared", (byte) (sheared ? 1 : 0)));
		
		return out;
	}
	
	public boolean sheared;
}
