package mingecraft.silverbird.server.protocol.packets.world;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet34MultiBlockChange extends Packet {

	int chunkX, chunkZ;
	short[] coordArr;
	byte[] typeArr, metaArr;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		chunkX = dis.readInt();
		chunkZ = dis.readInt();
		short arrSize = dis.readShort();

		coordArr = new short[arrSize];
		typeArr = new byte[arrSize];
		metaArr = new byte[arrSize];
		
		for(int i = 0; i < arrSize; i++) {
			coordArr[i] = dis.readShort();
		}
		
		dis.readFully(typeArr);
		dis.readFully(metaArr);
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(chunkX);
		dos.writeInt(chunkZ);
		
		dos.writeShort(coordArr.length);
		
		for(int i = 0; i < coordArr.length; i++) {
			dos.writeShort(coordArr[i]);
		}
		
		dos.write(typeArr);
		dos.write(metaArr);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketSize() {
		return 10 + 4 * coordArr.length;
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 0x34;
	}

	@Override
	public String debug() {
		return "MultiBlockChange [cx: " + chunkX + "; cz: " + chunkZ + "; size: " + coordArr.length + "]";
	}

}
