package mingecraft.silverbird.server.entity.vehicle;

import com.flowpowered.nbt.ByteTag;
import com.flowpowered.nbt.CompoundMap;

import mingecraft.silverbird.server.entity.Entity;

public class EntityMinecart extends Entity {
	public EntityMinecart() {
		super();
	}
	
	@Override
	public void load(CompoundMap nbt) {
		super.load(nbt);
		
		type = (byte) nbt.get("Type").getValue();
	}
	
	public CompoundMap save() {
		CompoundMap out = super.save();
		
		out.put(new ByteTag("", type));
		
		return out;
	}
	
	public byte type;
}
