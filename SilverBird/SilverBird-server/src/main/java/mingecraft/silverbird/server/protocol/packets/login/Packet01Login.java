package mingecraft.silverbird.server.protocol.packets.login;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet01Login extends Packet {

	public int protocolVersion;
	public String username;
	public String password;
	public long seed;
	public byte dimension;
	
	@Override
	public void readPacketData(DataInputStream datainputstream) throws IOException {
		protocolVersion = datainputstream.readInt();
		username = datainputstream.readUTF();
		password = datainputstream.readUTF();
		seed = datainputstream.readLong();
		dimension = datainputstream.readByte();
	}

	@Override
	public void writePacketData(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.writeInt(protocolVersion);
		dataoutputstream.writeUTF(username);
		dataoutputstream.writeUTF(password);
		dataoutputstream.writeLong(seed);
		dataoutputstream.writeByte(dimension);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 4 + username.length() + password.length() + 5;
	}

	@Override
	public int getPacketId() {
		return 0x01;
	}

	@Override
	public String debug() {
		return "Login [protocol: " + protocolVersion + "; username: " + username + "; password: " + password + "; seed: " + seed + "; dimension: " + dimension + "]";
	}

}
