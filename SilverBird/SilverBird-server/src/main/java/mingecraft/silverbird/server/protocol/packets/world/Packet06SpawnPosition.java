package mingecraft.silverbird.server.protocol.packets.world;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet06SpawnPosition extends Packet {

	public int x, y, z;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		x = dis.readInt();
		y = dis.readInt();
		z = dis.readInt();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(x);
		dos.writeInt(y);
		dos.writeInt(z);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 12;
	}

	@Override
	public int getPacketId() {
		// TODO Auto-generated method stub
		return 6;
	}

	@Override
	public String debug() {
		return "SpawnPosition [X: " + x + "; Y: " + y + "; Z: " + z + "]";
	}

}
