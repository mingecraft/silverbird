package mingecraft.silverbird.server.entity.items;

import com.flowpowered.nbt.ByteTag;
import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.ShortTag;

import mingecraft.silverbird.server.entity.Entity;
import mingecraft.silverbird.server.entity.INBTSavable;

public class EntityProjectile extends Entity implements INBTSavable {
	public EntityProjectile() {
		super();
	}

	@Override
	public void load(CompoundMap nbt) {
		super.load(nbt);

		xTile = (short) nbt.get("xTile").getValue();
		yTile = (short) nbt.get("yTile").getValue();
		zTile = (short) nbt.get("zTile").getValue();

		inTile = (byte) nbt.get("inTile").getValue();
		shake = (byte) nbt.get("shake").getValue();

		inGround = (byte) nbt.get("inGround").getValue() != 0;
	}

	@Override
	public CompoundMap save() {
		CompoundMap out = super.save();

		out.put(new ShortTag("xTile", xTile));
		out.put(new ShortTag("yTile", yTile));
		out.put(new ShortTag("zTile", zTile));

		out.put(new ByteTag("inTile", inTile));
		out.put(new ByteTag("shake", shake));
		out.put(new ByteTag("inGround", (byte) (inGround ? 1 : 0)));

		return out;
	}

	public short xTile, yTile, zTile;
	public byte inTile;
	public byte shake;
	public boolean inGround;
}
