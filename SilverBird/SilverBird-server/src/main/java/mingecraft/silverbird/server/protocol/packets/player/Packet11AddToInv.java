package mingecraft.silverbird.server.protocol.packets.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.container.ItemStack;
import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet11AddToInv extends Packet {

	public ItemStack item;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		item = new ItemStack();
		item.id = dis.readShort();
		item.count = dis.readByte();
		item.damage = dis.readShort();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeShort(item.id);
		dos.writeByte(item.count);
		dos.writeShort(item.damage);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 5;
	}

	@Override
	public int getPacketId() {
		return 0x11;
	}

	@Override
	public String debug() {
		return "AddToInv [id: " + item.id + "; count: " + item.count + "; damage: " + item.damage + "]";
	}

}
