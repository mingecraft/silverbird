package mingecraft.silverbird.server.protocol.packets.entities;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet3BComplexEntity extends Packet {

	int x, z;
	short y;
	byte[] payload;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		x = dis.readInt();
		y = dis.readShort();
		z = dis.readInt();
		
		short size = dis.readShort();
		
		payload = new byte[size];
		dis.readFully(payload);
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(x);
		dos.writeShort(y);
		dos.writeInt(z);
		
		dos.writeShort(payload.length);
		dos.write(payload);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 12 + payload.length;
	}

	@Override
	public int getPacketId() {
		return 0x3B;
	}

	@Override
	public String debug() {
		return "ComplesEntity [x: " + x + "; y: " + y + "; z : " + z + "; size: " + payload.length + "]";
	}

}
