package mingecraft.silverbird.server.entity.mob;

import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.ShortTag;

import mingecraft.silverbird.server.entity.Entity;
import mingecraft.silverbird.server.entity.INBTSavable;

public class EntityMob extends Entity implements INBTSavable {
	public EntityMob() {
		super();
	}
	
	public void load(CompoundMap nbt) {
		super.load(nbt);

		attackTime = (short) nbt.get("AttackTime").getValue();
		deathTime = (short) nbt.get("DeathTime").getValue();
		health = (short) nbt.get("Health").getValue();
		hurtTime = (short) nbt.get("HurtTime").getValue();
	}
	
	public CompoundMap save() {
		CompoundMap out = super.save();
		
		out.put(new ShortTag("AttackTime", attackTime));
		
		return out;
	}
	
	public short attackTime;
	public short deathTime;
	public short health;
	public short hurtTime;
}
