package mingecraft.silverbird.server.world;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.CompoundTag;
import com.flowpowered.nbt.IntTag;
import com.flowpowered.nbt.LongTag;
import com.flowpowered.nbt.stream.NBTInputStream;
import com.flowpowered.nbt.stream.NBTOutputStream;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.server.entity.Entity;
import mingecraft.silverbird.server.world.chunks.Chunk;
import mingecraft.silverbird.server.world.chunks.ChunkCoordinates;
import mingecraft.silverbird.server.world.chunks.ChunkLoader;

/**
 * Handle world loading and all the shit.
 * 
 * @author maxoufox
 *
 */
public class World {
	private static Logger l = Logger.getLogger("World");

	private File saveDir;
	private String name;

	private ChunkLoader loader;

	private int spawnX = 0;
	private int spawnY = 16;
	private int spawnZ = 0;
	private long time = 6000;
	private long randomSeed = (new Random()).nextLong();
	private long lastPlayed = System.currentTimeMillis();
	private long sizeOnDisk = 1;
	
	private HashMap<ChunkCoordinates, Chunk> chuncks = new HashMap<ChunkCoordinates, Chunk>();

	public List<Entity> entities;

	/**
	 * Load a world.
	 * 
	 * @param name
	 */
	public World(String name) {
		this.name = name;
		l.i("Loading world " + this.name + "...");

		this.saveDir = new File(name);

		if (!this.saveDir.exists()) {
			this.saveDir.mkdir();
		}

		try {
			NBTInputStream nbtis = new NBTInputStream(new FileInputStream(new File(this.saveDir, "level.dat")), true);
			CompoundTag nbt = (CompoundTag) nbtis.readTag();
			nbtis.close();
			
			CompoundTag data = (CompoundTag) nbt.getValue().get("Data");

			spawnX = ((IntTag) data.getValue().get("SpawnX")).getValue();
			spawnX = ((IntTag) data.getValue().get("SpawnY")).getValue();
			spawnX = ((IntTag) data.getValue().get("SpawnZ")).getValue();

			time = ((LongTag) data.getValue().get("Time")).getValue();
			randomSeed = ((LongTag) data.getValue().get("RandomSeed")).getValue();
			lastPlayed = ((LongTag) data.getValue().get("SizeOnDisk")).getValue();
			sizeOnDisk = ((LongTag) data.getValue().get("LastPlayed")).getValue();
		} catch (IOException | NullPointerException e) {
			l.e("Can't load level.dat!", e);
			l.i("Trying to save a new one...");
			saveLevelDat();
		}

		loader = new ChunkLoader(this, true);
		this.entities = new ArrayList<Entity>();

		// loader.load(1, 3);
		/*
		for(int x = -16; x <= 16; x++) {
			for(int z = -16; z <= 16; z++) {
				this.loadChunk(x, z);
			}
		}
		*/
	}
	
	public ChunkLoader getLoader() {
		return loader;
	}
	
	public void loadChunk(int x, int z) {
		chuncks.put(new ChunkCoordinates(x, z), loader.load(x, z));
	}

	/**
	 * 
	 * @return Save folder for the world.
	 */
	public File getSaveFolder() {
		return saveDir;
	}

	/**
	 * SAVES THE WORLD!
	 */
	public void save() {
		l.i("Saving world " + this.name + "...");
		this.saveLevelDat();
	}

	/**
	 * Saves the level.dat file.
	 */
	private void saveLevelDat() {
		
		CompoundMap datas = new CompoundMap();

		datas.put(new IntTag("SpawnX", spawnX));
		datas.put(new IntTag("SpawnY", spawnY));
		datas.put(new IntTag("SpawnZ", spawnZ));

		datas.put(new LongTag("Time", time));
		datas.put(new LongTag("RandomSeed", randomSeed));
		datas.put(new LongTag("SizeOnDisk", lastPlayed));
		datas.put(new LongTag("LastPlayed", sizeOnDisk));
		
		CompoundTag data = new CompoundTag("Data", datas);
		
		CompoundMap roots = new CompoundMap();
		roots.put(data);
		CompoundTag root = new CompoundTag("", roots);
		
		try {
			NBTOutputStream nbtos = new NBTOutputStream(new FileOutputStream(new File(this.saveDir, "level.dat")), true);
			
			nbtos.writeTag(root);
			
			nbtos.close();
		} catch (IOException e) {
			l.e("Can't save level.dat!", e);
		}
	}
}
