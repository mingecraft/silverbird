package mingecraft.silverbird.server.protocol.packets.entities.movement;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet1FEntityRelativeMove extends Packet {

	int eid;
	byte x, y, z;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		eid = dis.readInt();
		x = dis.readByte();
		y = dis.readByte();
		z = dis.readByte();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(eid);
		dos.writeByte(x);
		dos.writeByte(y);
		dos.writeByte(z);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 7;
	}

	@Override
	public int getPacketId() {
		return 0x1F;
	}

	@Override
	public String debug() {
		return "EntityRelMove [eid: " + eid + "; x: " + x + "; y: " + y + "; z: " + z + "]";
	}

}
