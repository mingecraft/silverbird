package mingecraft.silverbird.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import mingecraft.silverbird.net.util.Logger;

public class ServerConfig {
	public static final int PROTOCOL_VERSION = 6;
	
	public static int port = 25565;
	public static String ip = "127.0.0.1";
	public static String world = "world";
	
	private static Logger l = Logger.getLogger("Config");
	
	public static void load(String file) {
		l.i("Loading config " + file + "...");
		
		if ((new File(file)).exists()) {
			Properties prop = new Properties();
			
			try {
				FileInputStream input = new FileInputStream(file);
				prop.load(input);
			} catch (IOException e) {
				l.e("Can't load config " + file + ", staying with default values.", e);
			}
			
			port = Integer.parseInt(prop.getProperty("port", "25565"));
			ip = prop.getProperty("ip", "127.0.0.1");
			world = prop.getProperty("world", "world");
		} else {
			save(file);
		}
	}
	
	public static void save(String file) {
		l.i("Saving config " + file + "...");
		
		Properties prop = new Properties();
		
		prop.setProperty("port", Integer.toString(port));
		prop.setProperty("ip", ip);
		prop.setProperty("world", world);
		
		FileOutputStream out;
		try {
			out = new FileOutputStream(file);
			prop.store(out, "SilverBird configuration file.");
		} catch (IOException e) {
			l.e("Can't save config " + file + ", staying with default values.", e);
		}
		
	}
}
