package mingecraft.silverbird.server.entity;

import java.util.ArrayList;
import java.util.List;

import com.flowpowered.nbt.ByteTag;
import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.DoubleTag;
import com.flowpowered.nbt.FloatTag;
import com.flowpowered.nbt.ListTag;
import com.flowpowered.nbt.ShortTag;

public class Entity implements INBTSavable {
	public Entity() {

	}

	@Override
	@SuppressWarnings("unchecked")
	public void load(CompoundMap nbt) {
		// id = (String) (nbt.get("id")).getValue();

		List<DoubleTag> pos = (List<DoubleTag>) nbt.get("Pos").getValue();

		posX = pos.get(0).getValue();
		posY = pos.get(1).getValue();
		posZ = pos.get(2).getValue();

		List<DoubleTag> motion = (List<DoubleTag>) nbt.get("Motion").getValue();

		velX = motion.get(0).getValue();
		velY = motion.get(1).getValue();
		velZ = motion.get(2).getValue();

		List<FloatTag> rotation = (List<FloatTag>) nbt.get("Rotation").getValue();

		yaw = rotation.get(0).getValue();
		pitch = rotation.get(1).getValue();

		fallDistance = (float) nbt.get("FallDistance").getValue();
		fire = (short) nbt.get("Fire").getValue();
		air = (short) nbt.get("Air").getValue();
		onGround = (byte) nbt.get("OnGround").getValue() != 0;
	}

	@Override
	public CompoundMap save() {
		CompoundMap out = new CompoundMap();

		// out.put(new StringTag("id", id));

		List<DoubleTag> posData = new ArrayList<DoubleTag>();
		posData.add(new DoubleTag("", posX));
		posData.add(new DoubleTag("", posY));
		posData.add(new DoubleTag("", posZ));
		out.put(new ListTag<DoubleTag>("Pos", DoubleTag.class, posData));

		List<DoubleTag> velData = new ArrayList<DoubleTag>();
		velData.add(new DoubleTag("", velX));
		velData.add(new DoubleTag("", velY));
		velData.add(new DoubleTag("", velZ));
		out.put(new ListTag<DoubleTag>("Motion", DoubleTag.class, velData));

		List<FloatTag> rotData = new ArrayList<FloatTag>();
		rotData.add(new FloatTag("", yaw));
		rotData.add(new FloatTag("", pitch));
		out.put(new ListTag<FloatTag>("Rotation", FloatTag.class, rotData));

		out.put(new FloatTag("FallDistance", fallDistance));
		out.put(new ShortTag("Fire", fire));
		out.put(new ShortTag("Air", air));
		out.put(new ByteTag("OnGround", (byte) (onGround ? 1 : 0)));

		return out;
	}

	public String id;
	public double posX;
	public double posY;
	public double posZ;

	public double velX;
	public double velY;
	public double velZ;

	public float yaw;
	public float pitch;

	public float fallDistance;
	public short fire;
	public short air;
	public boolean onGround;
}
