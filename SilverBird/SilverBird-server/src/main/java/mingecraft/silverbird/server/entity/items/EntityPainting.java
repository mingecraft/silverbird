package mingecraft.silverbird.server.entity.items;

import com.flowpowered.nbt.CompoundMap;

import mingecraft.silverbird.server.entity.Entity;
import mingecraft.silverbird.server.entity.INBTSavable;

public class EntityPainting extends Entity implements INBTSavable {
	public EntityPainting() {
		super();
	}

	public enum Direction {
		UNDEFINED(-1), EAST(0), NORTH(1), WEST(2), SOUTH(3);

		byte id;

		Direction(int id) {
			this.id = (byte) id;
		}

		public byte getId() {
			return id;
		}

		public static Direction fromId(int id) {
			switch (id) {
			case 0:
				return EAST;
			case 1:
				return NORTH;
			case 2:
				return WEST;
			case 3:
				return SOUTH;
			default:
				return UNDEFINED;
			}
		}
	}

	public void load(CompoundMap nbt) {
		super.load(nbt);
		
		dir = Direction.fromId((int) nbt.get("Dir").getValue());
		motive = (String) nbt.get("Motive").getValue();

		tileX = (short) nbt.get("TileX").getValue();
		tileY = (short) nbt.get("TileY").getValue();
		tileZ = (short) nbt.get("TileZ").getValue();
		
	}

	public CompoundMap save() {
		CompoundMap out = super.save();

		return out;
	}
	
	public Direction dir;
	public String motive;
	public short tileX, tileY, tileZ;
}
