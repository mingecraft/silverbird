package mingecraft.silverbird.server.protocol.packets.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet0EDigging extends Packet {

	byte status, y, face;
	int x, z;

	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		status = dis.readByte();
		x = dis.readInt();
		y = dis.readByte();
		z = dis.readInt();
		face = dis.readByte();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeByte(status);
		dos.writeInt(x);
		dos.writeByte(y);
		dos.writeInt(z);
		dos.writeByte(face);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 11;
	}

	@Override
	public int getPacketId() {
		return 0x0E;
	}

	@Override
	public String debug() {
		return "Digging [status: " + status + "; x: " + x + "; y: " + y + "; z: " + z + "; face: " + face + "]";
	}

}
