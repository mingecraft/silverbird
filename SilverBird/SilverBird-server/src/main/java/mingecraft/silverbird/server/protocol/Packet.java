package mingecraft.silverbird.server.protocol;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;

/**
 * Packet base class.
 * 
 * @author maxoufox
 *
 */
public abstract class Packet {

	/**
	 * Reads the packet
	 * 
	 * @param datainputstream Input Stream.
	 * @throws IOException If error
	 */
	public abstract void readPacketData(DataInputStream dis) throws IOException;

	/**
	 * Writes packet
	 * 
	 * @param dataoutputstream Output Stream.
	 * @throws IOException If error
	 */
	public abstract void writePacketData(DataOutputStream dos) throws IOException;

	/**
	 * Handle the packet.
	 * 
	 * @param nethandler Handler.
	 * @param player     The player.
	 * @throws IOException
	 */
	public abstract void processPacket(INetHandler nethandler, Player player) throws IOException;

	/**
	 * Get packet size.
	 * 
	 * @return Packet size.
	 */
	public abstract int getPacketSize();

	/**
	 * Get packet id
	 * 
	 * @return Packet id.
	 */
	public abstract int getPacketId();

	public abstract String debug();
}
