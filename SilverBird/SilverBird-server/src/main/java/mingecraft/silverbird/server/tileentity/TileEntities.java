package mingecraft.silverbird.server.tileentity;

import java.util.HashMap;

import mingecraft.silverbird.net.util.Logger;

public class TileEntities {
	private static HashMap<String, Class<? extends TileEntity>> entities = new HashMap<String, Class<? extends TileEntity>>();

	private static Logger l = Logger.getLogger("Entities");

	public static Class<? extends TileEntity> getEntity(String name) {
		if (entities.containsKey(name)) {
			return entities.get(name);
		} else {
			l.w("Requested unknown tile entity type " + name + "!");
			return null;
		}
	}

	public static void registerEntity(String name, Class<? extends TileEntity> entity) {
		if (entities.containsKey(name)) {
			l.w("Overwritting tile entitiy " + name + " from " + entities.get(name).getCanonicalName() + " to "
					+ entity.getCanonicalName());
		}
		
		l.d("Registering tile entity " + name + " -> " + entity.getCanonicalName());
		
		entities.put(name, entity);
	}
}
