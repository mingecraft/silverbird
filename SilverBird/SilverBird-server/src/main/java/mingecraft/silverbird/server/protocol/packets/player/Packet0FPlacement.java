package mingecraft.silverbird.server.protocol.packets.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet0FPlacement extends Packet {

	short id;
	int x, z;
	byte y, direction;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		id = dis.readShort();
		x = dis.readInt();
		y = dis.readByte();
		z = dis.readInt();
		direction = dis.readByte();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeShort(id);
		dos.writeInt(x);
		dos.writeByte(y);
		dos.writeInt(z);
		dos.writeByte(direction);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 12;
	}

	@Override
	public int getPacketId() {
		return 0x0F;
	}

	@Override
	public String debug() {
		return "BlockPlacement [id: " + id + "; x: " + x + "; y: " + y + "; z: " + z + "; dir: " + direction + "]";
	}

}
