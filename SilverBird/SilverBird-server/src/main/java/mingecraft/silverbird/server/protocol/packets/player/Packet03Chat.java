package mingecraft.silverbird.server.protocol.packets.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet03Chat extends Packet {

	public String message;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		message = dis.readUTF();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeUTF(message);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return message.length() + 2;
	}

	@Override
	public int getPacketId() {
		return 3;
	}

	@Override
	public String debug() {
		return "Chat [message: " + message + "]";
	}

}
