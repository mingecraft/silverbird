package mingecraft.silverbird.server.protocol.packets.entities;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet3CExplosion extends Packet {

	double x, y, z;
	float radius;
	byte[] records;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		x = dis.readDouble();
		y = dis.readDouble();
		z = dis.readDouble();
		
		radius = dis.readFloat();
		
		int count = dis.readInt();
		records = new byte[count * 3];
		dis.readFully(records);
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeDouble(x);
		dos.writeDouble(y);
		dos.writeDouble(z);
		dos.writeFloat(radius);
		
		dos.writeInt(records.length / 3);
		dos.write(records);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 32 + records.length * 3;
	}

	@Override
	public int getPacketId() {
		return 0x3C;
	}

	@Override
	public String debug() {
		return "Explosion [x: " + x + "; y: " + y + "; z: " + z + "; r: " + radius + "; count: " + records.length / 3 + "]";
	}

}
