package mingecraft.silverbird.server.protocol.packets.entities.movement;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet20EntityLook extends Packet {

	int eid;
	byte yaw, pitch;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		eid = dis.readInt();
		yaw = dis.readByte();
		pitch = dis.readByte();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(eid);
		dos.writeByte(yaw);
		dos.writeByte(pitch);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 6;
	}

	@Override
	public int getPacketId() {
		return 0x20;
	}

	@Override
	public String debug() {
		return "EntityLook [eid: " + eid + "; yaw: " + yaw + "; pitch: " + pitch + "]";
	}

}
