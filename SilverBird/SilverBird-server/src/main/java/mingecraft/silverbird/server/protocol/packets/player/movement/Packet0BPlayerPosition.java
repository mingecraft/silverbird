package mingecraft.silverbird.server.protocol.packets.player.movement;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet0BPlayerPosition extends Packet {

	public double x, y, z, stance;
	public boolean onGround;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		x = dis.readDouble();
		y = dis.readDouble();
		stance = dis.readDouble();
		z = dis.readDouble();
		onGround = dis.readBoolean();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeDouble(x);
		dos.writeDouble(y);
		dos.writeDouble(stance);
		dos.writeDouble(z);
		dos.writeBoolean(onGround);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 33;
	}

	@Override
	public int getPacketId() {
		return 0x0B;
	}

	@Override
	public String debug() {
		return "PlayerPos [x: " + x + "; y: " + y + "; z: " + z + "; stance: " + stance + "; onGround: " + onGround + "]";
	}

}
