package mingecraft.silverbird.server.protocol.packets.entities;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet14NamedEntitySpawn extends Packet {

	public int eid;
	public int x, y, z;
	public String name;
	public byte rotation, pitch;
	public short item;

	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		eid = dis.readInt();
		name = dis.readUTF();
		x = dis.readInt();
		y = dis.readInt();
		z = dis.readInt();
		rotation = dis.readByte();
		pitch = dis.readByte();
		item = dis.readShort();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(eid);
		dos.writeUTF(name);
		dos.writeInt(x);
		dos.writeInt(y);
		dos.writeInt(z);
		dos.writeByte(rotation);
		dos.writeByte(pitch);
		dos.writeShort(item);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 22 + name.length();
	}

	@Override
	public int getPacketId() {
		return 0x14;
	}

	@Override
	public String debug() {
		return "NamedEntitySpawn [eid: " + eid + "; name: " + name + "; x: " + x + "; y: " + y + "; z: " + z + "; rot: "
				+ rotation + "; pitch: " + pitch + "; item: " + item + "]";
	}

}
