package mingecraft.silverbird.server.world.chunks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.flowpowered.nbt.ByteArrayTag;
import com.flowpowered.nbt.ByteTag;
import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.CompoundTag;
import com.flowpowered.nbt.IntTag;
import com.flowpowered.nbt.ListTag;
import com.flowpowered.nbt.LongTag;
import com.flowpowered.nbt.stream.NBTInputStream;
import com.flowpowered.nbt.stream.NBTOutputStream;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.server.entity.Entities;
import mingecraft.silverbird.server.entity.Entity;
import mingecraft.silverbird.server.tileentity.TileEntities;
import mingecraft.silverbird.server.tileentity.TileEntity;
import mingecraft.silverbird.server.world.World;

/**
 * Handles chunks.
 * 
 * @author maxoufox
 *
 */
public class Chunk {
	private static Logger l = Logger.getLogger("World");

	private ChunkCoordinates coords;

	private byte[] blocks = new byte[32768];
	private byte[] data = new byte[32768];
	private byte[] skyLight = new byte[32768];
	private byte[] blockLight = new byte[32768];

	private byte[] heightMap = new byte[256];

	private long lastUpdate = System.currentTimeMillis();
	private boolean terrainPopulated = true;

	private World world;
	private File chunck;

	private ArrayList<TileEntity> tileentities = new ArrayList<TileEntity>();

	/**
	 * Loads chunk.
	 * 
	 * @param chunck Chunk file.
	 */
	public Chunk(World world, File chunck) {
		this.world = world;
		this.chunck = chunck;

		if (chunck.exists()) {

			l.d("Loading chunck " + chunck.getPath() + "...");

			try {
				NBTInputStream nbtis = new NBTInputStream(new FileInputStream(chunck));
				CompoundTag root = (CompoundTag) nbtis.readTag();
				nbtis.close();

				CompoundTag level = (CompoundTag) root.getValue().get("Level");

				coords = new ChunkCoordinates(((IntTag) level.getValue().get("xPos")).getValue(),
						((IntTag) level.getValue().get("zPos")).getValue());

				blocks = ((ByteArrayTag) level.getValue().get("Blocks")).getValue();

				data = unpackHalfBytes(((ByteArrayTag) level.getValue().get("Data")).getValue());
				skyLight = unpackHalfBytes(((ByteArrayTag) level.getValue().get("SkyLight")).getValue());
				blockLight = unpackHalfBytes(((ByteArrayTag) level.getValue().get("BlockLight")).getValue());

				heightMap = ((ByteArrayTag) level.getValue().get("HeightMap")).getValue();

				lastUpdate = (long) level.getValue().get("LastUpdate").getValue();
				terrainPopulated = (byte) level.getValue().get("TerrainPopulated").getValue() == 1;

				@SuppressWarnings("unchecked")
				List<CompoundTag> entitiesList = (List<CompoundTag>) level.getValue().get("Entities").getValue();

				entitiesList.forEach(new Consumer<CompoundTag>() {
					@Override
					public void accept(CompoundTag entity) {
						String type = (String) entity.getValue().get("id").getValue();

						Class<? extends Entity> e = Entities.getEntity(type);
						if (e != null) {
							try {
								Entity einstance = e.newInstance();
								einstance.load(entity.getValue());
								world.entities.add(einstance);
								l.d("Loaded entity " + type + " (" + einstance.posX + "; " + einstance.posY + "; "
										+ einstance.posZ + ").");
							} catch (InstantiationException | IllegalAccessException e1) {
								l.e("Error while loading entity " + type + "!", e1);
							}
						}
					}
				});

				tileentities = new ArrayList<TileEntity>();

				@SuppressWarnings("unchecked")
				List<CompoundTag> tileentitiesList = (List<CompoundTag>) level.getValue().get("TileEntities")
						.getValue();

				tileentitiesList.forEach(new Consumer<CompoundTag>() {
					@Override
					public void accept(CompoundTag tileentity) {
						String type = (String) tileentity.getValue().get("id").getValue();

						Class<? extends TileEntity> te = TileEntities.getEntity(type);
						try {
							TileEntity teinstance = te.newInstance();
							teinstance.load(tileentity.getValue());
							tileentities.add(teinstance);
							l.d("Loaded tileentity " + type + " (" + teinstance.x + "; " + teinstance.y + "; "
									+ teinstance.z + ").");
						} catch (InstantiationException | IllegalAccessException e) {
							l.e("Error while loading tileentity " + type + "!", e);
						}
					}
				});

			} catch (IOException e) {
				l.e("Can't load chunck " + chunck.getPath() + " !", e);
			}
		} else {
			l.d("Generating chunck " + chunck.getPath() + "...");
			for(int x = 0; x < 16; x++) {
				for(int y = 0; y < 128; y++) {
					for(int z = 0; z < 16; z++) {
						int i = y + (z * 128 + (x * 2048));
						
						blocks[i] = 0;
						data[i] = 0;
						skyLight[i] = 15;
						blockLight[i] = 15;
						
						if (y == 0)
							blocks[i] = 7;
						else if (y >= 1 && y < 6)
							blocks[i] = 1;
						else if (y >= 6 && y < 8)
							blocks[i] = 3;
						else if (y == 8)
							blocks[i] = 2;
						
						
					}
				}
			}
			this.save();
		}
	}
	
	public ChunkCoordinates getCoords() {
		return this.coords;
	}

	public void save() {
		l.d("Saving chunck " + chunck.getPath() + "...");

		CompoundMap items = new CompoundMap();

		items.put(new IntTag("xPos", coords.x));
		items.put(new IntTag("zPos", coords.z));

		items.put(new ByteArrayTag("Blocks", blocks));

		items.put(new ByteArrayTag("Data", packHalfBytes(data)));
		items.put(new ByteArrayTag("SkyLight", packHalfBytes(skyLight)));
		items.put(new ByteArrayTag("BlockLight", packHalfBytes(blockLight)));

		items.put(new ByteArrayTag("HeightMap", heightMap));

		items.put(new LongTag("LastUpdate", lastUpdate));
		items.put(new ByteTag("TerrainPopulated", (byte) (terrainPopulated ? 1 : 0)));

		List<CompoundTag> tilesentities = new ArrayList<CompoundTag>();

		tileentities.forEach(new Consumer<TileEntity>() {
			@Override
			public void accept(TileEntity te) {
				tilesentities.add(new CompoundTag("", te.save()));
			}
		});

		items.put(new ListTag<CompoundTag>("TileEntities", CompoundTag.class, tilesentities));

		List<CompoundTag> entities = new ArrayList<CompoundTag>();

		world.entities.forEach(new Consumer<Entity>() {
			@Override
			public void accept(Entity e) {
				if (e.posX > coords.x * 16 && e.posX < (coords.x + 1) * 16) {
					if (e.posZ > coords.z * 16 && e.posZ < (coords.z + 1) * 16) {
						entities.add(new CompoundTag("", e.save()));
					}
				}
			}
		});

		items.put(new ListTag<CompoundTag>("Entities", CompoundTag.class, tilesentities));

		CompoundTag level = new CompoundTag("Level", items);

		CompoundMap rootitems = new CompoundMap();
		rootitems.put(level);

		CompoundTag root = new CompoundTag("", rootitems);

		try {
			NBTOutputStream out = new NBTOutputStream(new FileOutputStream(this.chunck));

			out.writeTag(root);

			out.close();
		} catch (IOException e1) {
			l.e("Can't save chunck " + chunck.getPath() + " !", e1);
		}
	}

	private byte[] packHalfBytes(byte[] in) {
		byte[] out = new byte[in.length / 2];

		for (int i = 0; i < out.length; i++) {
			out[i] = (byte) (in[i * 2] << 4 | in[i * 2 + 1]);
		}

		return out;
	}

	private byte[] unpackHalfBytes(byte[] in) {
		byte[] out = new byte[in.length * 2];

		for (int i = 0; i < in.length; i++) {
			byte d = in[i];

			byte b1 = (byte) ((d >> 4) & 0x0F);
			byte b2 = (byte) (d & 0x0F);

			out[i * 2] = b1;
			out[i * 2 + 1] = b2;
		}

		return out;
	}
}
