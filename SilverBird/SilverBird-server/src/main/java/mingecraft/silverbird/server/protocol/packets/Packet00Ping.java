package mingecraft.silverbird.server.protocol.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet00Ping extends Packet {

	@Override
	public void readPacketData(DataInputStream datainputstream) throws IOException {
		
	}

	@Override
	public void writePacketData(DataOutputStream dataoutputstream) throws IOException {
		
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) {
		
	}

	@Override
	public int getPacketSize() {
		return 0;
	}

	@Override
	public int getPacketId() {
		return 0;
	}

	@Override
	public String debug() {
		return "Ping []";
	}

}
