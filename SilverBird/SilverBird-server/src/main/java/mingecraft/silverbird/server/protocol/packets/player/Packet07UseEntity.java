package mingecraft.silverbird.server.protocol.packets.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet07UseEntity extends Packet {

	public int usereid, targeteid;
	public boolean leftClick;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		usereid = dis.readInt();
		targeteid = dis.readInt();
		leftClick = dis.readBoolean();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(usereid);
		dos.writeInt(targeteid);
		dos.writeBoolean(leftClick);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 9;
	}

	@Override
	public int getPacketId() {
		return 7;
	}

	@Override
	public String debug() {
		return "UseEntity [" + usereid + "->" + targeteid + " left: " + leftClick + "]";
	}

}
