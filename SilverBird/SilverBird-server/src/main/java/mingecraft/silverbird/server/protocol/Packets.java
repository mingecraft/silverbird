package mingecraft.silverbird.server.protocol;

import java.io.IOException;
import java.util.HashMap;

import mingecraft.silverbird.net.util.Logger;

public class Packets {
	private static HashMap<Integer, Class<? extends Packet>> idToClass = new HashMap<Integer, Class<? extends Packet>>();
	private static HashMap<Class<? extends Packet>, Integer> classToId = new HashMap<Class<? extends Packet>, Integer>();
	
	private static Logger l = Logger.getLogger("Packet");
	
	public static Class<? extends Packet> getPacket(int id) throws IOException {
		if (idToClass.containsKey(id)) {
			return idToClass.get(id);
		} else {
			throw new IOException("Unknown packet id " + id + "!");
		}
	}
	
	public static void addPacket(Class<? extends Packet> clazz) {
		int id = -1;
		try {
			id = clazz.newInstance().getPacketId();
		} catch (InstantiationException | IllegalAccessException e) {
			l.e("Error", e);
		}
		
		if (idToClass.containsKey(id)) {
			l.w("Overwritting packet id " + id + " from " + idToClass.get(id).getCanonicalName() + " to " + clazz.getCanonicalName());

			classToId.remove(idToClass.get(id));
			idToClass.remove(id);
			
		}

		l.d("Registering packet " + id + " -> " + clazz.getCanonicalName());
		
		idToClass.put(id, clazz);
		classToId.put(clazz, id);
	}
}
