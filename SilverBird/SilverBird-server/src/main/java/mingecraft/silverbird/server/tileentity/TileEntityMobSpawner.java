package mingecraft.silverbird.server.tileentity;

import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.ShortTag;
import com.flowpowered.nbt.StringTag;

import mingecraft.silverbird.server.entity.INBTSavable;

public class TileEntityMobSpawner extends TileEntity implements INBTSavable {
	public TileEntityMobSpawner() {
		super();
	}
	
	@Override
	public void load(CompoundMap nbt) {
		entityId = (String) nbt.get("EntityId").getValue();
		delay = (short) nbt.get("Delay").getValue();
	}
	
	@Override
	public CompoundMap save() {
		CompoundMap out = super.save();
		
		out.put(new StringTag("EntityId", entityId));
		out.put(new ShortTag("Delay", delay));
		
		return out;
	}
	
	public String entityId;
	public short delay;
}
