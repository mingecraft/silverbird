package mingecraft.silverbird.server.container;

import com.flowpowered.nbt.ByteTag;
import com.flowpowered.nbt.CompoundMap;

import mingecraft.silverbird.server.entity.INBTSavable;

public class ItemStackInv extends ItemStack implements INBTSavable {
	public ItemStackInv() {
		super();
	}
	
	@Override
	public void load(CompoundMap data) {
		super.load(data);
		
		slot = (byte) data.get("Slot").getValue();
	}
	
	@Override
	public CompoundMap save() {
		CompoundMap out = super.save();
		
		out.put(new ByteTag("Slot", slot));
		
		return out;
	}
	
	public byte slot;
}
