package mingecraft.silverbird.server.world.chunks;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

import mingecraft.silverbird.server.world.World;

/**
 * Load chunks.
 * 
 * @author maxoufox
 *
 */
public class ChunkLoader {

	private World world;
	private boolean createIfNecessary;

	/**
	 * Constructor
	 * 
	 * @param world             The world
	 * @param createIfNecessary Create chunks?
	 */
	public ChunkLoader(World world, boolean createIfNecessary) {
		this.world = world;
		this.createIfNecessary = createIfNecessary;
	}
	
	/**
	 * Load a chunk.
	 * @param x X position of the chunk.
	 * @param z Z position of the chunk.
	 * @return
	 */
	public Chunk load(int x, int z) {
		return new Chunk(this.world, chunkFileForXZ(x, z));
	}
	
	/**
	 * Loop trough all existing chuncks.
	 * @param action
	 */
	public void forEach(BiConsumer<ChunkCoordinates, Chunk> action) {
		try {
			Files.walk(world.getSaveFolder().toPath())
			.filter(Files::isRegularFile).filter(new Predicate<Path>() {
				@Override
				public boolean test(Path t) {
					// TODO: Better check!
					return t.getFileName().toString().startsWith("c.") && t.getFileName().toString().endsWith(".dat");
				}
			}).forEach(new Consumer<Path>() {

				@Override
				public void accept(Path p) {
					Chunk c = new Chunk(world, p.toFile());
					action.accept(c.getCoords(), c);
				}
				
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Get file for a chunk
	 * 
	 * @param x X position
	 * @param z Z position
	 * @return A File.
	 */
	private File chunkFileForXZ(int x, int z) {
		String chunckFile = (new StringBuilder()).append("c.").append(Integer.toString(x, 36)).append(".")
				.append(Integer.toString(z, 36)).append(".dat").toString();
		String xFolder = Integer.toString(x & 0x3f, 36);
		String zFolder = Integer.toString(z & 0x3f, 36);
		File file = new File(world.getSaveFolder(), xFolder);
		if (!file.exists()) {
			if (createIfNecessary) {
				file.mkdir();
			} else {
				return null;
			}
		}
		file = new File(file, zFolder);
		if (!file.exists()) {
			if (createIfNecessary) {
				file.mkdir();
			} else {
				return null;
			}
		}
		file = new File(file, chunckFile);
		if (!file.exists() && !createIfNecessary) {
			return null;
		} else {
			return file;
		}
	}

}
