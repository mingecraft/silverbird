package mingecraft.silverbird.server.protocol.packets.player.movement;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet0CPlayerLook extends Packet {

	public float yaw, pitch;
	public boolean onGround;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		yaw = dis.readFloat();
		pitch = dis.readFloat();
		onGround = dis.readBoolean();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeFloat(yaw);
		dos.writeFloat(pitch);
		dos.writeBoolean(onGround);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 9;
	}

	@Override
	public int getPacketId() {
		return 0x0C;
	}

	@Override
	public String debug() {
		return "PlayerLook [onGround: " + onGround + "; yaw: " + yaw + "; pitch: " + pitch + "]";
	}

}
