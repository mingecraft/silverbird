package mingecraft.silverbird.server.protocol.packets.world;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet35BlockChange extends Packet {
	
	public int x, z;
	public byte y, id, meta;

	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		x = dis.readInt();
		y = dis.readByte();
		z = dis.readInt();
		
		id = dis.readByte();
		meta = dis.readByte();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(x);
		dos.writeByte(y);
		dos.writeInt(z);
		
		dos.writeByte(id);
		dos.writeByte(meta);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public int getPacketSize() {
		return 11;
	}

	@Override
	public int getPacketId() {
		return 0x35;
	}

	@Override
	public String debug() {
		return "BlockChange [x: " + x + "; y: " + y + "; z: " + z + "; block: " + id + ":" + meta + "]";
	}

}
