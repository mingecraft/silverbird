package mingecraft.silverbird.server.protocol.packets.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.container.DummyInventory;
import mingecraft.silverbird.server.container.IInventory;
import mingecraft.silverbird.server.container.ItemStackInv;
import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet05PlayerInventory extends Packet {

	public int invtype;
	public short count;
	public IInventory inv;
	private int payloadSize = 0;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		invtype = dis.readInt();
		count = dis.readShort();
		
		inv = new DummyInventory(count, invtype);
		
		for(byte i = 0; i < count; i++) {
			short id = dis.readShort();
			payloadSize += 2;
			
			if (id != -1) {
				byte count = dis.readByte();
				payloadSize += 1;
				
				short damage = dis.readShort();
				payloadSize += 2;
				
				ItemStackInv item = new ItemStackInv();
				
				item.id = id;
				item.count = count;
				item.damage = damage;
				item.slot = i;
				
				inv.setItem(item);
			} else {
				inv.destroyItem(i);
			}
		}
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(invtype);
		dos.writeShort(count);
		
		for(byte i = 0; i < inv.getSize(); i++) {
			ItemStackInv item = inv.getItemInSlot(i);
			
			if (item != null) {
				dos.writeShort(item.id);
				dos.writeByte(item.count);
				dos.writeShort(item.damage);
			} else {
				dos.writeShort(-1);
			}
		}
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 6 + payloadSize;
	}

	@Override
	public int getPacketId() {
		return 5;
	}

	@Override
	public String debug() {
		return "PlayerInventory [" + invtype + "]";
	}
}
