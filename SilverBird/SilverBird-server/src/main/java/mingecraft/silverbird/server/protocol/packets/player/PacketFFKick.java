package mingecraft.silverbird.server.protocol.packets.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class PacketFFKick extends Packet {

	public String reason;

	@Override
	public void readPacketData(DataInputStream datainputstream) throws IOException {
		reason = datainputstream.readUTF();
	}

	@Override
	public void writePacketData(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.writeUTF(reason);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 2 + reason.length();
	}

	@Override
	public int getPacketId() {
		return 0xFF;
	}

	@Override
	public String debug() {
		return "Kick [reason: " + reason + "]";
	}

}
