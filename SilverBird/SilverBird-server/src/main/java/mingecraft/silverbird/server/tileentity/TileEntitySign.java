package mingecraft.silverbird.server.tileentity;

import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.StringTag;

import mingecraft.silverbird.server.entity.INBTSavable;

public class TileEntitySign extends TileEntity implements INBTSavable {
	public TileEntitySign() {
		super();
	}

	@Override
	public void load(CompoundMap data) {
		super.load(data);
		
		text = new String[4];

		text[0] = (String) data.get("Text1").getValue();
		text[1] = (String) data.get("Text2").getValue();
		text[2] = (String) data.get("Text3").getValue();
		text[3] = (String) data.get("Text4").getValue();
	}

	@Override
	public CompoundMap save() {
		CompoundMap out = super.save();

		out.put(new StringTag("Text1", text[0]));
		out.put(new StringTag("Text2", text[1]));
		out.put(new StringTag("Text3", text[2]));
		out.put(new StringTag("Text4", text[3]));
		
		return out;
	}
	
	String[] text;
}
