package mingecraft.silverbird.server.protocol.packets.world;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet33MapChunk extends Packet {

	public int x, z;
	public short y;
	public byte sizeX, sizeY, sizeZ;
	public byte[] data;

	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		x = dis.readInt();
		y = dis.readShort();
		z = dis.readInt();
		sizeX = dis.readByte();
		sizeY = dis.readByte();
		sizeZ = dis.readByte();
		int compressedSize = dis.readInt();
		data = new byte[compressedSize];
		dis.readFully(data);
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(x);
		dos.writeShort(y);
		dos.writeInt(z);

		dos.writeByte(sizeX);
		dos.writeByte(sizeY);
		dos.writeByte(sizeZ);

		dos.writeInt(data.length);
		dos.write(data);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 17 + data.length;
	}

	@Override
	public int getPacketId() {
		return 0x33;
	}

	@Override
	public String debug() {
		return "MapChunk [x: " + x + "; y: " + y + "; z: " + z + "; size: " + sizeX + "x" + sizeY + "x" + sizeZ
				+ "; size: " + data.length + "]";
	}

}
