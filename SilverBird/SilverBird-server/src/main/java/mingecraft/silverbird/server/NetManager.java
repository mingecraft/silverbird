package mingecraft.silverbird.server;

import java.io.IOException;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.net.util.interfaces.INetManager;
import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.protocol.Packet;
import mingecraft.silverbird.server.protocol.Packets;

public class NetManager extends INetManager {
	private Logger pl = Logger.getLogger("Packet");
	
	Logger l = Logger.getLogger("Client Handler");
	Player player = null;
	public boolean online = true;

	@Override
	public void handle() {
		player = new Player(this);
		
		l.d("Accepted connection from " + this.client.getInetAddress().toString() + ".");
		
		while (online && !this.client.isClosed()) {
			try {
				int id = this.dis.read();
				
				if (id == -1)
					break;
				
				Packet p = Packets.getPacket(id).newInstance();
				p.readPacketData(this.dis);
				
				pl.d("[C->S] " + p.debug());
				
				p.processPacket(player.netHandler, player);
				
				
			} catch (IOException | InstantiationException | IllegalAccessException e) {
				l.e("Error while handling client", e);
				
				try {
					player.kick("Exception : " + e.getMessage());
				} catch (IOException e1) {
					l.e("Error while kicking client!", e1);
				}
				
				return;
			}
			
		}
		l.d("Closed connection with " + this.client.getInetAddress().toString() + ".");
	}

	public void clean() {
		
	}
}
