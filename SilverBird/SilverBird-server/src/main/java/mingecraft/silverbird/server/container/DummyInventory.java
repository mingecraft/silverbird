package mingecraft.silverbird.server.container;

public class DummyInventory implements IInventory {

	int size = 0;
	int type = 0;
	
	ItemStackInv[] items;
	
	public DummyInventory(int size, int type) {
		this.size = size;
		this.type = type;
		this.items = new ItemStackInv[size];
	}
	
	@Override
	public int getSize() {
		return size;
	}

	@Override
	public ItemStackInv getItemInSlot(int slot) {
		return items[slot];
	}

	@Override
	public void setItem(ItemStackInv item) {
		items[item.slot] = item;
	}

	@Override
	public int getType() {
		return type;
	}

	@Override
	public void destroyItem(int slot) {
		items[slot] = null;
	}

}
