package mingecraft.silverbird.server.protocol.packets.entities;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet27AttachEntity extends Packet {

	int playereid;
	int vehicleeid;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		playereid = dis.readInt();
		vehicleeid = dis.readInt();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(playereid);
		dos.writeInt(vehicleeid);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 8;
	}

	@Override
	public int getPacketId() {
		return 0x27;
	}

	@Override
	public String debug() {
		return "AttachEntity [Peid: " + playereid + "; Veid: " + vehicleeid + "]";
	}

}
