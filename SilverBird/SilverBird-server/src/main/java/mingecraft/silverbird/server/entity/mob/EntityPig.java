package mingecraft.silverbird.server.entity.mob;

import com.flowpowered.nbt.ByteTag;
import com.flowpowered.nbt.CompoundMap;

import mingecraft.silverbird.server.entity.Entity;
import mingecraft.silverbird.server.entity.INBTSavable;

public class EntityPig extends Entity implements INBTSavable {
	public EntityPig() {
		super();
	}
	
	public void load(CompoundMap nbt) {
		super.load(nbt);

		saddle = (byte) nbt.get("Saddle").getValue() != 0;
	}
	
	public CompoundMap save() {
		CompoundMap out = super.save();
		
		out.put(new ByteTag("Saddle", (byte) (saddle ? 1 : 0)));
		
		return out;
	}
	
	public boolean saddle;
}
