package mingecraft.silverbird.server.protocol.packets.entities.movement;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet1CEntityVelocity extends Packet {

	int eid;
	short x, y, z;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		eid = dis.readInt();
		x = dis.readShort();
		y = dis.readShort();
		z = dis.readShort();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(eid);
		dos.writeShort(x);
		dos.writeShort(y);
		dos.writeShort(z);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 10;
	}

	@Override
	public int getPacketId() {
		return 0x1C;
	}

	@Override
	public String debug() {
		return "EntityVelocity [eid: " + eid + "; x: " + x + "; y: " + y + "; z: " + z + "]";
	}

}
