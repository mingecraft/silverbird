package mingecraft.silverbird.server.protocol.packets.entities;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet15PickupSpawn extends Packet {

	public int eid, x, y, z;
	public short item;
	public byte count, rotation, pitch, roll;

	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		eid = dis.readInt();
		item = dis.readShort();
		count = dis.readByte();
		x = dis.readInt();
		y = dis.readInt();
		z = dis.readInt();
		rotation = dis.readByte();
		pitch = dis.readByte();
		roll = dis.readByte();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(eid);
		dos.writeShort(item);
		dos.writeByte(count);
		dos.writeInt(x);
		dos.writeInt(y);
		dos.writeInt(z);
		dos.writeByte(rotation);
		dos.writeByte(pitch);
		dos.writeByte(roll);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 22;
	}

	@Override
	public int getPacketId() {
		return 0x15;
	}

	@Override
	public String debug() {
		return "PickupSpawn [eid: " + eid + "; item: " + item + "x" + count + "; x: " + x + "; y: " + y + "; z: " + z
				+ "; rot: " + rotation + "; pitch: " + pitch + "; roll: " + roll + "]";
	}

}
