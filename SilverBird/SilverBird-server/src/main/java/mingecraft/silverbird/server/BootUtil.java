package mingecraft.silverbird.server;

import java.util.Scanner;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.server.entity.Entities;
import mingecraft.silverbird.server.entity.items.EntityItem;
import mingecraft.silverbird.server.entity.items.EntityPainting;
import mingecraft.silverbird.server.entity.items.EntityProjectile;
import mingecraft.silverbird.server.entity.mob.EntityMob;
import mingecraft.silverbird.server.entity.mob.EntityPig;
import mingecraft.silverbird.server.entity.mob.EntitySheep;
import mingecraft.silverbird.server.entity.mob.EntitySlime;
import mingecraft.silverbird.server.protocol.Packets;
import mingecraft.silverbird.server.protocol.packets.Packet00Ping;
import mingecraft.silverbird.server.protocol.packets.entities.Packet12Animation;
import mingecraft.silverbird.server.protocol.packets.entities.Packet14NamedEntitySpawn;
import mingecraft.silverbird.server.protocol.packets.entities.Packet15PickupSpawn;
import mingecraft.silverbird.server.protocol.packets.entities.Packet16CollectItem;
import mingecraft.silverbird.server.protocol.packets.entities.Packet17AddVehicle;
import mingecraft.silverbird.server.protocol.packets.entities.Packet18MobSpawn;
import mingecraft.silverbird.server.protocol.packets.entities.Packet1DDestroEntity;
import mingecraft.silverbird.server.protocol.packets.entities.Packet1EEntity;
import mingecraft.silverbird.server.protocol.packets.entities.Packet26EntityStatus;
import mingecraft.silverbird.server.protocol.packets.entities.Packet27AttachEntity;
import mingecraft.silverbird.server.protocol.packets.entities.Packet3BComplexEntity;
import mingecraft.silverbird.server.protocol.packets.entities.Packet3CExplosion;
import mingecraft.silverbird.server.protocol.packets.entities.movement.Packet1CEntityVelocity;
import mingecraft.silverbird.server.protocol.packets.entities.movement.Packet1FEntityRelativeMove;
import mingecraft.silverbird.server.protocol.packets.entities.movement.Packet20EntityLook;
import mingecraft.silverbird.server.protocol.packets.entities.movement.Packet21EntityLookRelMove;
import mingecraft.silverbird.server.protocol.packets.entities.movement.Packet22EntityTeleport;
import mingecraft.silverbird.server.protocol.packets.login.Packet01Login;
import mingecraft.silverbird.server.protocol.packets.login.Packet02Handshake;
import mingecraft.silverbird.server.protocol.packets.player.Packet03Chat;
import mingecraft.silverbird.server.protocol.packets.player.Packet05PlayerInventory;
import mingecraft.silverbird.server.protocol.packets.player.Packet07UseEntity;
import mingecraft.silverbird.server.protocol.packets.player.Packet08UpdateHealth;
import mingecraft.silverbird.server.protocol.packets.player.Packet09Respawn;
import mingecraft.silverbird.server.protocol.packets.player.Packet0EDigging;
import mingecraft.silverbird.server.protocol.packets.player.Packet0FPlacement;
import mingecraft.silverbird.server.protocol.packets.player.Packet10HoldingChange;
import mingecraft.silverbird.server.protocol.packets.player.Packet11AddToInv;
import mingecraft.silverbird.server.protocol.packets.player.PacketFFKick;
import mingecraft.silverbird.server.protocol.packets.player.movement.Packet0APlayer;
import mingecraft.silverbird.server.protocol.packets.player.movement.Packet0BPlayerPosition;
import mingecraft.silverbird.server.protocol.packets.player.movement.Packet0CPlayerLook;
import mingecraft.silverbird.server.protocol.packets.player.movement.Packet0DPlayerPositionLook;
import mingecraft.silverbird.server.protocol.packets.world.Packet04TimeUpdate;
import mingecraft.silverbird.server.protocol.packets.world.Packet06SpawnPosition;
import mingecraft.silverbird.server.protocol.packets.world.Packet32PreChunk;
import mingecraft.silverbird.server.protocol.packets.world.Packet33MapChunk;
import mingecraft.silverbird.server.protocol.packets.world.Packet34MultiBlockChange;
import mingecraft.silverbird.server.protocol.packets.world.Packet35BlockChange;
import mingecraft.silverbird.server.tileentity.TileEntities;
import mingecraft.silverbird.server.tileentity.TileEntityMobSpawner;
import mingecraft.silverbird.server.tileentity.TileEntitySign;
import mingecraft.silverbird.server.tileentity.containers.TileEntityChest;
import mingecraft.silverbird.server.tileentity.containers.TileEntityFurnace;

public class BootUtil {
	private static Logger l = Logger.getLogger("Main");
	
	public static void printBoot() {
		Scanner sc = new Scanner(BootUtil.class.getResourceAsStream("/boot.txt"));
		while(sc.hasNext()) {
			l.i(sc.nextLine());
		}
		sc.close();
	}
	
	public static void registerPackets() {
		Packets.addPacket(Packet00Ping.class);
		Packets.addPacket(Packet01Login.class);
		Packets.addPacket(Packet02Handshake.class);
		Packets.addPacket(Packet03Chat.class);
		Packets.addPacket(Packet04TimeUpdate.class);
		Packets.addPacket(Packet05PlayerInventory.class);
		Packets.addPacket(Packet06SpawnPosition.class);
		Packets.addPacket(Packet07UseEntity.class);
		Packets.addPacket(Packet08UpdateHealth.class);
		Packets.addPacket(Packet09Respawn.class);
		Packets.addPacket(Packet0APlayer.class);
		Packets.addPacket(Packet0BPlayerPosition.class);
		Packets.addPacket(Packet0CPlayerLook.class);
		Packets.addPacket(Packet0DPlayerPositionLook.class);
		Packets.addPacket(Packet0EDigging.class);
		Packets.addPacket(Packet0FPlacement.class);
		Packets.addPacket(Packet10HoldingChange.class);
		Packets.addPacket(Packet11AddToInv.class);
		Packets.addPacket(Packet12Animation.class);
		Packets.addPacket(Packet14NamedEntitySpawn.class);
		Packets.addPacket(Packet15PickupSpawn.class);
		Packets.addPacket(Packet16CollectItem.class);
		Packets.addPacket(Packet17AddVehicle.class);
		Packets.addPacket(Packet18MobSpawn.class);
		Packets.addPacket(Packet1CEntityVelocity.class);;
		Packets.addPacket(Packet1DDestroEntity.class);
		Packets.addPacket(Packet1EEntity.class);
		Packets.addPacket(Packet1FEntityRelativeMove.class);
		Packets.addPacket(Packet20EntityLook.class);
		Packets.addPacket(Packet21EntityLookRelMove.class);
		Packets.addPacket(Packet22EntityTeleport.class);
		Packets.addPacket(Packet26EntityStatus.class);
		Packets.addPacket(Packet27AttachEntity.class);
		Packets.addPacket(Packet32PreChunk.class);
		Packets.addPacket(Packet33MapChunk.class);
		Packets.addPacket(Packet34MultiBlockChange.class);
		Packets.addPacket(Packet35BlockChange.class);
		Packets.addPacket(Packet3BComplexEntity.class);
		Packets.addPacket(Packet3CExplosion.class);
		Packets.addPacket(PacketFFKick.class);
	}
	
	public static void registerEntities() {
		Entities.registerEntity("Mob", EntityMob.class);
		Entities.registerEntity("Monster", EntityMob.class);
		Entities.registerEntity("Creeper", EntityMob.class);
		Entities.registerEntity("Skeleton", EntityMob.class);
		Entities.registerEntity("Spider", EntityMob.class);
		Entities.registerEntity("Giant", EntityMob.class);
		Entities.registerEntity("Zombie", EntityMob.class);
		Entities.registerEntity("Slime", EntitySlime.class);
		Entities.registerEntity("PigZombie", EntityMob.class);
		Entities.registerEntity("Ghast", EntityMob.class);

		Entities.registerEntity("Pig", EntityPig.class);
		Entities.registerEntity("Sheep", EntitySheep.class);
		Entities.registerEntity("Cow", EntityMob.class);
		Entities.registerEntity("Chicken", EntityMob.class);

		Entities.registerEntity("Item", EntityItem.class);
		Entities.registerEntity("Arrow", EntityProjectile.class);
		Entities.registerEntity("Snowball", EntityProjectile.class);
		Entities.registerEntity("Painting", EntityPainting.class);

		TileEntities.registerEntity("Furnace", TileEntityFurnace.class);
		TileEntities.registerEntity("Sign", TileEntitySign.class);
		TileEntities.registerEntity("MobSpawner", TileEntityMobSpawner.class);
		TileEntities.registerEntity("Chest", TileEntityChest.class);
	}
}
