package mingecraft.silverbird.server.protocol.packets.entities;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet17AddVehicle extends Packet {

	public int eid;
	public int x, y, z;
	public byte type;

	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		eid = dis.readInt();
		type = dis.readByte();
		x = dis.readInt();
		y = dis.readInt();
		z = dis.readInt();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(eid);
		dos.writeByte(type);
		dos.writeInt(x);
		dos.writeInt(y);
		dos.writeInt(z);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 17;
	}

	@Override
	public int getPacketId() {
		return 0x17;
	}

	@Override
	public String debug() {
		return "AddVehicle [eid: " + eid + "; type: " + type + "; x: " + x + "; y: " + y + "; z: " + z + "]";
	}

}
