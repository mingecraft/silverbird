package mingecraft.silverbird.server.player.net;

import java.io.IOException;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.server.ServerConfig;
import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.protocol.Packet;
import mingecraft.silverbird.server.protocol.packets.login.Packet01Login;
import mingecraft.silverbird.server.protocol.packets.login.Packet02Handshake;

/**
 * Handles the login sequence.
 * 
 * @author maxoufox
 *
 */
public class LoginNetHandler implements INetHandler {
	private Logger pl = Logger.getLogger("Packet");

	@Override
	public void handle(Packet packet, Player player) throws IOException {
		if (packet instanceof Packet02Handshake) {
			
			Packet02Handshake p = (Packet02Handshake) packet;
			player.name = p.name;

			Packet02Handshake r = new Packet02Handshake();
			r.name = "-";
			player.sendPacket(r);
			
		} else if (packet instanceof Packet01Login) {
			
			Packet01Login p = (Packet01Login) packet;

			if (p.protocolVersion != ServerConfig.PROTOCOL_VERSION) {
				if (p.protocolVersion > ServerConfig.PROTOCOL_VERSION) {
					player.kick("Outdated server!");
				} else {
					player.kick("Outdated client!");
				}
				return;
			}
			
			player.name = p.username;
			
			
			
			
		}
	}
}
