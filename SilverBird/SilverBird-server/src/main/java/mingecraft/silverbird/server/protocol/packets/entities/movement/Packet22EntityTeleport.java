package mingecraft.silverbird.server.protocol.packets.entities.movement;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet22EntityTeleport extends Packet {

	public int eid, x, y, z;
	public byte yaw, pitch;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		eid = dis.readInt();
		x = dis.readInt();
		y = dis.readInt();
		z = dis.readInt();
		yaw = dis.readByte();
		pitch = dis.readByte();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(eid);
		dos.writeInt(x);
		dos.writeInt(y);
		dos.writeInt(z);
		dos.writeByte(yaw);
		dos.writeByte(pitch);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 18;
	}

	@Override
	public int getPacketId() {
		return 0x22;
	}

	@Override
	public String debug() {
		return "EntityTeleport [eid: " + eid + "; x: " + x + "; y: " + y + "; z: " + z + "; yaw: " + yaw + "; pitch: " + pitch + "]";
	}

}