package mingecraft.silverbird.server.console;

import mingecraft.silberbird.console.ICommandHandler;
import mingecraft.silverbird.server.SilverBird;

public class ExitCommand implements ICommandHandler {

	@Override
	public boolean handle(String[] args) {
		if (args.length > 0)
			return false;
		
		SilverBird.getInstance().clean();
		
		return true;
	}

	@Override
	public String getCommand() {
		return "exit";
	}

	@Override
	public String getUsage() {
		return "exit";
	}

}
