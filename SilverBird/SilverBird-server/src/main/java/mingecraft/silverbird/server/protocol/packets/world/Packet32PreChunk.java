package mingecraft.silverbird.server.protocol.packets.world;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet32PreChunk extends Packet {

	public int x, z;
	public boolean mode;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		x = dis.readInt();
		z = dis.readInt();
		mode = dis.readBoolean();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeInt(x);
		dos.writeInt(z);
		dos.writeBoolean(mode);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 9;
	}

	@Override
	public int getPacketId() {
		return 0x32;
	}

	@Override
	public String debug() {
		return "PreChunk [X: " + x + "; Z: " + z + "; init: " + mode + "]";
	}

}
