package mingecraft.silverbird.server.entity;

import java.util.HashMap;

import mingecraft.silverbird.net.util.Logger;

public class Entities {
	private static HashMap<String, Class<? extends Entity>> entities = new HashMap<String, Class<? extends Entity>>();

	private static Logger l = Logger.getLogger("Entities");

	public static Class<? extends Entity> getEntity(String name) {
		if (entities.containsKey(name)) {
			return entities.get(name);
		} else {
			l.w("Requested unknown entity type " + name + "!");
			return null;
		}
	}

	public static void registerEntity(String name, Class<? extends Entity> entity) {
		if (entities.containsKey(name)) {
			l.w("Overwritting entitiy " + name + " from " + entities.get(name).getCanonicalName() + " to "
					+ entity.getCanonicalName());
		}
		
		l.d("Registering entity " + name + " -> " + entity.getCanonicalName());
		
		entities.put(name, entity);
	}
}
