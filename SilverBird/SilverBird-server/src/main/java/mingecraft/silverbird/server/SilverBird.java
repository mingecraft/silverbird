package mingecraft.silverbird.server;

import java.io.IOException;

import mingecraft.silberbird.console.Console;
import mingecraft.silverbird.net.TCPServer;
import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.net.util.interfaces.ICleanable;
import mingecraft.silverbird.server.console.ExitCommand;
import mingecraft.silverbird.server.world.World;

/**
 * Minecraft alpha 1.2.6 server
 * 
 * @author maxoufox
 *
 */
public class SilverBird implements ICleanable {
	private static SilverBird instance = new SilverBird();

	private TCPServer server;
	private Console console;
	private World world;
	private Logger l = Logger.getLogger("Main");

	/**
	 * Constructor.
	 */
	private SilverBird() {

	}

	/**
	 * Starts the server.
	 */
	public void run() {
		BootUtil.printBoot();
		l.i("Starting SilverBird version 0.0.1...");
		
		ServerConfig.load("server.properties");
		
		BootUtil.registerEntities();
		
		world = new World(ServerConfig.world);

		BootUtil.registerPackets();
		
		server = new TCPServer(ServerConfig.ip, ServerConfig.port, NetManager.class);
		server.start();

		console = new Console(System.in);

		console.addCommand(new ExitCommand());

		console.run();
	}

	/**
	 * Stops the server.
	 */
	@Override
	public void clean() {
		server.clean();
		console.clean();
		world.save();
		ServerConfig.save("server.properties");
	}

	/**
	 * Get an instance of SilverBird
	 * 
	 * @return a SilverBird instance.
	 */
	public static SilverBird getInstance() {
		return instance;
	}

	/**
	 * Main function. Program entry point.
	 * 
	 * @param args The args given to the program
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		instance.run();
	}
}
