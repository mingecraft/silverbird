package mingecraft.silverbird.server.protocol.packets.login;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

/**
 * Handler for the Handshake packet.
 * @author maxoufox
 *
 */
public class Packet02Handshake extends Packet {

	public String name;
	
	@Override
	public void readPacketData(DataInputStream datainputstream) throws IOException {
		name = datainputstream.readUTF();
	}

	@Override
	public void writePacketData(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.writeUTF(name);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return name.length() + 2;
	}

	@Override
	public int getPacketId() {
		return 0x02;
	}

	@Override
	public String debug() {
		return "Handshake [name: " + name + "]";
	}

}
