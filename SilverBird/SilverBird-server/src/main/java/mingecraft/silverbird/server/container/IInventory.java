package mingecraft.silverbird.server.container;

public interface IInventory {
	/**
	 * Get the size of the inventory
	 * 
	 * @return inventory size
	 */
	public int getSize();

	/**
	 * Get inventory type (for the networking!)
	 * 
	 * Main inventory: -1
	 * Equipped armor: -2
	 * Crafting slots: -3
	 * 
	 * @return inventory type.
	 *
	 */
	public int getType();
	
	/**
	 * Get an item from a slot.
	 * 
	 * @param slot Slot to request item from
	 * @return The item in the slot, or null if no item was there.
	 */
	public ItemStackInv getItemInSlot(int slot);

	/**
	 * Set an item
	 * 
	 * @param item Item to put.
	 */
	public void setItem(ItemStackInv item);
	
	/**
	 * Set a slot as empty
	 * @param slot The slot.
	 */
	public void destroyItem(int slot);
}
