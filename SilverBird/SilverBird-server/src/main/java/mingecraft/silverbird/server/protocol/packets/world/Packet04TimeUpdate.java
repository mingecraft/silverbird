package mingecraft.silverbird.server.protocol.packets.world;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;

public class Packet04TimeUpdate extends Packet {

	public long time;
	
	@Override
	public void readPacketData(DataInputStream dis) throws IOException {
		time = dis.readLong();
	}

	@Override
	public void writePacketData(DataOutputStream dos) throws IOException {
		dos.writeLong(time);
	}

	@Override
	public void processPacket(INetHandler nethandler, Player player) throws IOException {
		nethandler.handle(this, player);
	}

	@Override
	public int getPacketSize() {
		return 8;
	}

	@Override
	public int getPacketId() {
		return 4;
	}

	@Override
	public String debug() {
		return "Time [time: " + time + "]";
	}

}
