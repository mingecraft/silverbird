package mingecraft.silverbird.server.tileentity.containers;

import java.util.ArrayList;
import java.util.List;

import com.flowpowered.nbt.CompoundMap;
import com.flowpowered.nbt.CompoundTag;
import com.flowpowered.nbt.ListTag;
import com.flowpowered.nbt.ShortTag;

import mingecraft.silverbird.server.container.IInventory;
import mingecraft.silverbird.server.container.ItemStackInv;
import mingecraft.silverbird.server.entity.INBTSavable;
import mingecraft.silverbird.server.tileentity.TileEntity;

public class TileEntityFurnace extends TileEntity implements INBTSavable, IInventory {

	private ItemStackInv[] content;

	public short burnTime;
	public short cookTime;

	public enum SlotName {
		COOKING(0), FUEL(1), COOKED(2);

		private int id;

		SlotName(int id) {
			this.id = id;
		}

		int getId() {
			return this.id;
		}
	}

	public TileEntityFurnace() {
		super();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void load(CompoundMap data) {
		super.load(data);

		content = new ItemStackInv[getSize()];

		List<CompoundTag> items = (List<CompoundTag>) data.get("Items").getValue();

		for (CompoundTag i : items) {
			ItemStackInv item = new ItemStackInv();
			item.load(i.getValue());

			content[item.slot] = item;
		}

		burnTime = (short) data.get("BurnTime").getValue();
		cookTime = (short) data.get("CookTime").getValue();
	}

	public CompoundMap save() {
		CompoundMap out = super.save();

		List<CompoundTag> items = new ArrayList<CompoundTag>();

		for (ItemStackInv i : content) {
			if (i == null)
				continue;

			items.add(new CompoundTag("", i.save()));
		}

		out.put(new ListTag<CompoundTag>("Items", CompoundTag.class, items));

		out.put(new ShortTag("BurnTime", burnTime));
		out.put(new ShortTag("CookTime", cookTime));

		return out;
	}

	@Override
	public int getSize() {
		return 3;
	}

	@Override
	public ItemStackInv getItemInSlot(int slot) {
		return content[slot];
	}

	@Override
	public void setItem(ItemStackInv item) {
		content[item.slot] = item;
	}

	@Override
	public int getType() {
		return 0;
	}

	@Override
	public void destroyItem(int slot) {
		content[slot] = null;
	}
}
