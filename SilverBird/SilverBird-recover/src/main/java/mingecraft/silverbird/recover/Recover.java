package mingecraft.silverbird.recover;

import java.util.function.BiConsumer;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.server.BootUtil;
import mingecraft.silverbird.server.ServerConfig;
import mingecraft.silverbird.server.world.World;
import mingecraft.silverbird.server.world.chunks.Chunk;
import mingecraft.silverbird.server.world.chunks.ChunkCoordinates;

public class Recover {
	
	private static Recover instance = new Recover();

	private World world;
	private Logger l = Logger.getLogger("Main");
	
	public Recover() {
		
	}
	
	/**
	 * Starts the recoverer.
	 */
	public void run() {
		BootUtil.printBoot();
		l.i("Starting Recover version 1.0.0...");
		
		ServerConfig.load("server.properties");
		BootUtil.registerEntities();

		world = new World(ServerConfig.world);
		
		world.getLoader().forEach(new BiConsumer<ChunkCoordinates, Chunk>() {
			
			public void accept(ChunkCoordinates t, Chunk u) {
				l.d(t.x + ";" + t.z);
			}
		});
	}
	
	public static void main(String[] args) {
		instance.run();
	}
}
