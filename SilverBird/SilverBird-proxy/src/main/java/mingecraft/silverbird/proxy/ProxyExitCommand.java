package mingecraft.silverbird.proxy;

import mingecraft.silberbird.console.ICommandHandler;
import mingecraft.silverbird.server.SilverBird;


public class ProxyExitCommand implements ICommandHandler {
	@Override
	public boolean handle(String[] args) {
		if (args.length > 0)
			return false;

		SilverProxy.getInstance().clean();

		return true;
	}

	@Override
	public String getCommand() {
		return "exit";
	}

	@Override
	public String getUsage() {
		return "exit";
	}

}
