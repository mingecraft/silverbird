package mingecraft.silverbird.proxy.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.proxy.ProxyPlayer;
import mingecraft.silverbird.server.protocol.Packet;
import mingecraft.silverbird.server.protocol.Packets;

public class TCPClient extends Thread {
	private Logger l;

	ProxyPlayer player;
	Socket socket;
	
	public boolean debug = false;

	DataOutputStream dos = null;
	DataInputStream dis = null;

	String ip;
	int port;

	public TCPClient(ProxyPlayer player, String ip, int port) throws IOException {
		this.l = Logger.getLogger("TCPClient#" + this.getId());

		this.player = player;
		this.socket = new Socket();

		this.ip = ip;
		this.port = port;

		l.d("Init!");

		this.socket.connect(new InetSocketAddress(ip, port));

		dos = new DataOutputStream(this.socket.getOutputStream());
		dis = new DataInputStream(this.socket.getInputStream());
	}

	public void sendPacket(Packet p) throws IOException {
		synchronized (dos) {
			int id = p.getPacketId();

			if (debug)
				l.d("[C->S] " + p.debug());
			
			dos.writeByte(id);

			p.writePacketData(dos);
		}
	}

	public void close() throws IOException {
		synchronized (socket) {/*
			PacketFFKick disconnect = new PacketFFKick();
			disconnect.reason = "[SB] Swapping server...";
			this.sendPacket(disconnect);*/
			this.socket.close();
		}
	}

	public void run() {
		l.d("Run started!");
		try {
			while (!this.socket.isClosed()) {
				try {
					int id = dis.read();
					
					if (id == -1)
						break;

					Packet p = Packets.getPacket(id).newInstance();
					p.readPacketData(this.dis);

					player.netHandler.handleSC(p, player);
					
					if (debug)
						l.d("[S->C] " + p.debug());

				} catch (SocketException e) {
					
				} catch (IOException | InstantiationException | IllegalAccessException e) {
					l.e("Exception: ", e);
					player.kick("Exception : " + e.getMessage());
				}
			}
		} catch (IOException e) {
			try {
				player.kick("Exception : " + e.getMessage());
			} catch (IOException e1) {
			}
		}
		l.d("Run ended!");
	}
}
