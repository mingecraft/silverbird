package mingecraft.silverbird.proxy.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import mingecraft.silverbird.net.util.Logger;

public class ConfigServerList {
	private static Logger l = Logger.getLogger("ProxyConfig");
	
	public static HashMap<String, Server> serverList = new HashMap<String, Server>();
	
	public static void debug() {
		serverList.forEach(new BiConsumer<String, Server>() {
			@Override
			public void accept(String t, Server u) {
				l.d(t + " -> " + u.ip + ":" + u.port);
			}
		});
	}
	
	public static void load(String file) {
		l.i("Loading config " + file + "...");
		
		if ((new File(file)).exists()) {
			final Properties prop = new Properties();
			
			try {
				FileInputStream input = new FileInputStream(file);
				prop.load(input);
			} catch (IOException e) {
				l.e("Can't load config " + file + ", staying with default values.", e);
			}
			
			prop.keySet().forEach(new Consumer<Object>() {
				@Override
				public void accept(Object o) {
					String name = String.class.cast(o);
					
					String raw = prop.getProperty(name);
					
					String[] rawip = raw.split(":");
					
					Server srv = new Server();
					srv.name = name;
					srv.ip = rawip[0];
					srv.port = Integer.parseInt(rawip[1]);
					serverList.put(name, srv);
				}
			});
			
		} else {
			Server srv = new Server();
			srv.name = "main";
			srv.ip = "127.0.0.1";
			srv.port = 25566;
			serverList.put("main", srv);
			
			Server srv2 = new Server();
			srv2.name = "test2";
			srv2.ip = "127.0.0.1";
			srv2.port = 25567;
			serverList.put("test2", srv2);
			
			save(file);
		}
		
		ConfigServerList.debug();
	}
	
	public static void save(String file) {
		l.i("Saving config " + file + "...");
		
		final Properties prop = new Properties();
		
		serverList.forEach(new BiConsumer<String, Server>() {
			@Override
			public void accept(String name, Server srv) {
				prop.setProperty(name, srv.ip + ":" + srv.port);
			}
		});
		
		FileOutputStream out;
		try {
			out = new FileOutputStream(file);
			prop.store(out, "SilverBird configuration file.");
		} catch (IOException e) {
			l.e("Can't save config " + file + ", staying with default values.", e);
		}
		
	}
}
