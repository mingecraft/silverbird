/**
 * 
 */
package mingecraft.silverbird.proxy;

import mingecraft.silberbird.console.Console;
import mingecraft.silverbird.net.TCPServer;
import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.net.util.interfaces.ICleanable;
import mingecraft.silverbird.proxy.config.ConfigServerList;
import mingecraft.silverbird.proxy.net.ProxyNetManager;
import mingecraft.silverbird.server.BootUtil;
import mingecraft.silverbird.server.ServerConfig;

/**
 * Kinda like BungeeCord thing.
 * 
 * @author maxoufox
 *
 */
public class SilverProxy implements ICleanable {
	private static SilverProxy instance = new SilverProxy();

	private TCPServer server;
	private Console console;
	private Logger l = Logger.getLogger("Main");

	private SilverProxy() {

	}

	/**
	 * Starts the proxy.
	 */
	public void run() {
		BootUtil.printBoot();
		l.i("Starting SilverProxy version 1.0.0...");
		
		ConfigServerList.load("proxy.properties");
		
		ServerConfig.load("server.properties");
		
		BootUtil.registerPackets();
		
		server = new TCPServer(ServerConfig.ip, ServerConfig.port, ProxyNetManager.class);
		server.start();
		
		console = new Console(System.in);

		console.addCommand(new ProxyExitCommand());

		console.run();
	}

	/**
	 * Get an instance of SilverProxy
	 * 
	 * @return a SilverBird instance.
	 */
	public static SilverProxy getInstance() {
		return instance;
	}
	
	/**
	 * Program entry point
	 * 
	 * @param args Program args.
	 */
	public static void main(String[] args) {
		instance.run();
	}

	public void clean() {
		server.clean();
		console.clean();
		
		ConfigServerList.save("proxy.properties");
		ServerConfig.save("server.properties");
	}

}
