package mingecraft.silverbird.proxy;

import java.io.IOException;
import java.util.ArrayList;

import mingecraft.silverbird.proxy.config.Server;
import mingecraft.silverbird.proxy.net.ProxyNetHandler;
import mingecraft.silverbird.proxy.net.ProxyNetManager;
import mingecraft.silverbird.proxy.net.TCPClient;
import mingecraft.silverbird.server.protocol.Packet;
import mingecraft.silverbird.server.protocol.packets.entities.Packet1DDestroEntity;
import mingecraft.silverbird.server.protocol.packets.login.Packet02Handshake;
import mingecraft.silverbird.server.protocol.packets.player.Packet03Chat;
import mingecraft.silverbird.server.protocol.packets.player.PacketFFKick;

public class ProxyPlayer {
	public String name;
	public ProxyNetManager netManager;
	public ProxyNetHandler netHandler;
	public ArrayList<Integer> entities = new ArrayList<Integer>();
	public boolean isSwapping = false;

	/**
	 * Init the player
	 * 
	 * @param netManager Connection's net manager.
	 */
	public ProxyPlayer(ProxyNetManager netManager) {
		this.netManager = netManager;
		this.netHandler = new ProxyNetHandler();
	}

	/**
	 * Send a packet to the player
	 * 
	 * @param p Packet to send
	 * @throws IOException If error
	 */
	public void sendPacket(Packet p) throws IOException {
		synchronized (netManager.dos) {
			netManager.dos.writeByte(p.getPacketId());

			p.writePacketData(netManager.dos);
		}
	}

	/**
	 * Sends a chat message to the player
	 * 
	 * @param message Message to send
	 * @throws IOException If error.
	 */
	public void sendChat(String message) throws IOException {
		Packet03Chat p = new Packet03Chat();
		p.message = message;
		this.sendPacket(p);
	}

	public synchronized void swap(Server s) throws IOException {
		isSwapping = true;
		this.netManager.connection.close();
		this.unloadEntities();
		this.netManager.connection = new TCPClient(this, s.ip, s.port);
		this.netManager.connection.debug = false;
		this.netManager.connection.start();
		Packet02Handshake handshake = new Packet02Handshake();
		handshake.name = this.name;
		this.netManager.connection.sendPacket(handshake);
	}

	/**
	 * Unloads all entities client-side.
	 * 
	 * @throws IOException If error occurs.
	 */
	public void unloadEntities() throws IOException {
		for (int i = 0; i < this.entities.size(); i++) {
			Packet1DDestroEntity destroy = new Packet1DDestroEntity();
			destroy.eid = this.entities.get(i);
			this.sendPacket(destroy);
		}
		this.entities.clear();
	}

	/**
	 * Kicks the player.
	 * 
	 * @param reason Reason
	 * @throws IOException If error
	 */
	public void kick(String reason) throws IOException {
		PacketFFKick kick = new PacketFFKick();

		kick.reason = reason;

		this.sendPacket(kick);
		this.netManager.online = false;
		netManager.close();
	}
}
