package mingecraft.silverbird.proxy.net;

import java.io.IOException;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.net.util.interfaces.INetManager;
import mingecraft.silverbird.proxy.ProxyPlayer;
import mingecraft.silverbird.proxy.config.ConfigServerList;
import mingecraft.silverbird.server.protocol.Packet;
import mingecraft.silverbird.server.protocol.Packets;

/**
 * Handler net connections.
 * 
 * @author maxoufox
 *
 */
public class ProxyNetManager extends INetManager {
	Logger l = Logger.getLogger("Proxy Client Handler");
	
	public TCPClient connection;
	public ProxyPlayer player;
	public boolean online = true;

	@Override
	public void handle() {

		player = new ProxyPlayer(this);
		try {
			connection = new TCPClient(player, ConfigServerList.serverList.get("main").ip, ConfigServerList.serverList.get("main").port);
		} catch (IOException e2) {
			return;
		}
		connection.start();

		l.d("Accepted connection from " + this.client.getInetAddress().toString() + ".");

		while (online && !this.client.isClosed()) {
			try {
				int id = this.dis.read();
				if (id == -1)
					break;
				
				Packet p = Packets.getPacket(id).newInstance();
				p.readPacketData(this.dis);
				
				player.netHandler.handleCS(p, player);
				
			} catch (IOException | InstantiationException | IllegalAccessException e) {
				try {
					player.kick("Exception : " + e.getMessage());
				} catch (IOException e1) {
				}
				
				return;
			}

		}
		
		l.d("Connection closed with " + this.client.getInetAddress().toString() + ".");
	}

	public void clean() {

	}
}
