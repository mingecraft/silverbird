package mingecraft.silverbird.proxy.chat;

import java.io.IOException;
import java.util.Iterator;

import mingecraft.silverbird.proxy.ProxyPlayer;
import mingecraft.silverbird.proxy.config.ConfigServerList;
import mingecraft.silverbird.proxy.config.Server;

public class ChatCommandHandler {
	public ChatCommandHandler() {

	}

	public static boolean handle(String message, final ProxyPlayer player) throws IOException {
		if (message.startsWith("/p ")) {
			String[] args = message.split(" ");

			if (args.length >= 2) {
				switch (args[1]) {
				case "e": {
					String entitiesList = "";
					for (int i = 0; i < player.entities.size(); i++) {
						entitiesList += " " + player.entities.get(i);
					}

					player.sendChat("[SB] E:" + entitiesList);

					break;
				}
				case "ue": {
					player.unloadEntities();
					break;
				}
				case "ip": {
					if (args.length >= 3) {
						if (ConfigServerList.serverList.containsKey(args[2])) {
							player.sendChat("[SB] IP: " + ConfigServerList.serverList.get(args[2]).ip + ":"
									+ ConfigServerList.serverList.get(args[2]).port + ".");
						} else {
							player.sendChat("[SB] Unkown server \"" + args[2] + "\" !");
						}
					} else {
						player.sendChat("[SB] Missing argument!");
					}

					break;
				}
				case "s": {
					if (args.length >= 3) {
						if (ConfigServerList.serverList.containsKey(args[2])) {
							player.sendChat("[SB] Connecting to server \"" + args[2] + "\"...");
							Server s = ConfigServerList.serverList.get(args[2]);
							player.swap(s);
						} else {
							player.sendChat("[SB] Unkown server \"" + args[2] + "\" !");
						}
					} else {
						player.sendChat("[SB] Missing argument!");
					}

					break;
				}
				case "l": {
					String servers = "";
					int i = 0;

					Iterator<String> it = ConfigServerList.serverList.keySet().iterator();
					while (it.hasNext()) {
						if (i != 0) {
							servers += ", ";
						}

						servers += it.next();

						i++;
					}

					player.sendChat("[SB] S(" + i + "): " + servers);

					break;
				}
				default: {
					player.sendChat("[SB] Unknown subcommand \"" + args[1] + "\" !");
					break;
				}
				}
			}

			return true;
		} else {
			return false;
		}
	}
}
