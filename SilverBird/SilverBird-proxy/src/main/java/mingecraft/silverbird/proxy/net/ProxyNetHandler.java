package mingecraft.silverbird.proxy.net;

import java.io.IOException;

import mingecraft.silverbird.net.util.Logger;
import mingecraft.silverbird.proxy.ProxyPlayer;
import mingecraft.silverbird.proxy.chat.ChatCommandHandler;
import mingecraft.silverbird.server.player.Player;
import mingecraft.silverbird.server.player.net.INetHandler;
import mingecraft.silverbird.server.protocol.Packet;
import mingecraft.silverbird.server.protocol.packets.entities.Packet14NamedEntitySpawn;
import mingecraft.silverbird.server.protocol.packets.entities.Packet15PickupSpawn;
import mingecraft.silverbird.server.protocol.packets.entities.Packet17AddVehicle;
import mingecraft.silverbird.server.protocol.packets.entities.Packet18MobSpawn;
import mingecraft.silverbird.server.protocol.packets.entities.Packet1DDestroEntity;
import mingecraft.silverbird.server.protocol.packets.login.Packet01Login;
import mingecraft.silverbird.server.protocol.packets.login.Packet02Handshake;
import mingecraft.silverbird.server.protocol.packets.player.Packet03Chat;

public class ProxyNetHandler implements INetHandler {

	private Logger pl = Logger.getLogger("Packet");

	@Override
	public void handle(Packet packet, Player player) throws IOException {
		throw new IOException("DON'T USE THIS FOR NORMAL SERVER BLYAT");
	}

	/**
	 * Handles client to server packets.
	 * 
	 * @param p      Packet
	 * @param player Player
	 */
	public void handleCS(Packet p, ProxyPlayer player) {

		try {
			if (player.isSwapping) {
				if (p instanceof Packet01Login) {
					player.netManager.connection.sendPacket(p);
				}
			} else {
				if (p instanceof Packet02Handshake) {
					Packet02Handshake handshake = Packet02Handshake.class.cast(p);
					player.name = handshake.name;

					player.netManager.connection.sendPacket(p);
				} else if (p instanceof Packet03Chat) {
					Packet03Chat chat = Packet03Chat.class.cast(p);
					if (chat.message.startsWith("/login") || chat.message.startsWith("/register")) {
						pl.d("[maxoufox] Chat: <HIDDEN /login | /register command!>");
					} else {
						pl.d("[" + player.name + "] Chat: " + chat.message);
					}

					if (!ChatCommandHandler.handle(chat.message, player)) {
						player.netManager.connection.sendPacket(p);
					}
				} else {
					player.netManager.connection.sendPacket(p);
				}

				// pl.d("[" + player.name + "][C->S] " + p.debug());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Handles server to client packet.
	 * 
	 * @param p      Packet
	 * @param player Player
	 */
	public void handleSC(Packet p, ProxyPlayer player) {
		try {
			
			if (player.isSwapping) {
				if (p instanceof Packet02Handshake) {
					Packet02Handshake login = Packet02Handshake.class.cast(p);
					
					if (login.name.equals("-")) {
						Packet01Login handshake = new Packet01Login();
						handshake.protocolVersion = 6;
						handshake.username = player.name;
						handshake.password = "Password";
						handshake.seed = 0;
						handshake.dimension = 0;
						player.netManager.connection.sendPacket(handshake);
					} else {
						player.kick("[SB] Desired server requires auth!");
					}
				} else if (p instanceof Packet01Login) {
					player.isSwapping = false;
				}
				
				return;
			}

			if (p instanceof Packet14NamedEntitySpawn) {
				player.entities.add(Packet14NamedEntitySpawn.class.cast(p).eid);
			} else if (p instanceof Packet15PickupSpawn) {
				player.entities.add(Packet15PickupSpawn.class.cast(p).eid);
			} else if (p instanceof Packet17AddVehicle) {
				player.entities.add(Packet17AddVehicle.class.cast(p).eid);
			} else if (p instanceof Packet18MobSpawn) {
				player.entities.add(Packet18MobSpawn.class.cast(p).eid);
			} else if (p instanceof Packet1DDestroEntity) {
				int index = player.entities.indexOf(Packet1DDestroEntity.class.cast(p).eid);
				if (index != -1) {
					player.entities.remove(index);
				}
			}

			// pl.d("[" + player.name + "][S->C] " + p.debug());

			player.sendPacket(p);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
